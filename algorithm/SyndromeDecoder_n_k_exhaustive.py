import itertools

import numpy as np


class SyndromeDecoder_N_K_Exhaustive:
    def __init__(self, H, e, t, p, combinations):
        self.H = H
        self.e = e
        self.t = t
        self.p = p
        self.s = self.H.dot(self.e)
        self.matrix_lines = len(H)
        self.matrix_columns = len(H[0])
        self.H_transposed = np.transpose(self.H)

        self.f = self.calculate_f()
        self.pi, self.e_prime, self.permutation = self.get_phi()
        self.A_inverse = self.get_A_inverse()
        self.result = self.exhaust_solutions(combinations)

    def calculate_f(self):
        H_c = self.get_H_complementary()
        s_c = self.get_s_complementary()
        phi = self.H_transposed.dot(self.s)
        phi_c = H_c.dot(s_c)
        return phi + phi_c

    def get_H_complementary(self):
        H_ones = np.ones_like(self.H_transposed)
        return H_ones - self.H_transposed

    def get_s_complementary(self):
        s_of_ts = np.full_like(self.s, self.t)
        return s_of_ts - self.s

    def get_phi(self):
        # descending order
        permutation = np.argsort(-self.f)
        e_prime = np.take(self.e, permutation, axis=0)
        pi = np.take(self.H, permutation, axis=1)

        return pi, e_prime, permutation

    # attempts to reconstruct the permuted error vector
    def get_A_inverse_result(self, s):
        if self.A_inverse is not None:
            return np.round(self.A_inverse.dot(s))

    def get_A_inverse(self):
        A = self.pi[0:self.matrix_lines, 0:self.matrix_lines]
        det = np.linalg.det(A)
        if det != 0:
            return np.linalg.inv(A)

    # gets the count of 0 values in the first k positions
    def get_error_indices(self):
        sub_result = self.result[:self.matrix_lines]
        non_zeros = np.count_nonzero(sub_result)

        return self.t - non_zeros

    def are_results_binary_t_equal(self, result, t):
        if result is not None:
            v = np.zeros(self.matrix_columns, dtype=int)
            v[:len(result)] = result
            all_binary = np.logical_or(v == 0, v == 1).all() and np.count_nonzero(v) == t
            return all_binary
        return False

    def exhaust_solutions(self, combinations):
        result = self.get_A_inverse_result(self.s)
        if self.are_results_binary_t_equal(result, self.t):
            return 1
        elif self.are_results_binary_t_equal(result, self.t - self.p):
            return self.get_exhausted_solution(combinations)
        return 0

    def get_result_chances(self):
        return self.result

    def get_exhausted_solution(self, combinations):
        B = self.pi[:, self.matrix_lines:]
        for combination in combinations:
            e_ii = np.zeros(self.matrix_columns - self.matrix_lines, dtype=int)
            np.put(e_ii, combination, 1)
            s_prime = self.s - B.dot(e_ii)
            result_ii = self.get_A_inverse_result(s_prime)
            if self.are_results_binary_t_equal(result_ii, self.t - self.p):
                return 1
        return 0
