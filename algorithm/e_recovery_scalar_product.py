from matplotlib import pyplot as plt
import numpy as np
from tqdm import trange

NB_EXP = 20
NB_REPS = 20

for fault_amp in trange(10):

    success = np.empty(NB_EXP)

    for exp_index, N_raw in enumerate(np.logspace(3, 4.2, NB_EXP)):
        N = int(N_raw) // 64 * 64
        n, k, t = N, N // 2, int(np.sqrt(N))

        results = np.empty(NB_REPS)

        e = np.array(t * [1] + (n - t) * [0], dtype=np.float32)  # float32 for dot parallel computation
        for repeat in range(NB_REPS):
            # Parity-check matrix, generated 64 bits at a time
            H = np.random.randint(np.iinfo(np.int64).min,
                                  np.iinfo(np.int64).max,
                                  size=(n - k) * n // 64,
                                  dtype=np.int64).view(np.uint8)
            H = np.reshape(np.unpackbits(H), (n - k, n)).astype(np.float32)
            # print(np.unique(H, return_counts=True))
            # Error vector of weight t
            np.random.shuffle(e)
            # Syndrome computation
            s = np.matmul(H, e)
            s += np.random.randint(-fault_amp, fault_amp + 1, size=np.shape(s))
            # Complement of the parity check matrix
            H_c = 1 - H
            # Complement of the syndrome
            s_c = t - s
            # Computation of the cost function vector
            phi = np.dot(s, H) + np.dot(s_c, H_c)
            # Get the permutation that sorts phi
            perm = np.argsort(phi)
            # Show the last t elements of permuted e
            # to see if they are all 1
            # results[repeat] = np.argmax(e[perm] == 1.0)
            results[repeat] = np.sum(np.ones(t) == e[perm][-t:]) / t
        success[exp_index] = np.mean(results)
    plt.plot(np.logspace(2, 4, NB_EXP), success, label=f"$\Delta$ fault [-{fault_amp}, {fault_amp}]")
plt.ylim((0, 1.05))
plt.xlim((0, 10000))
plt.xlabel("n")
plt.ylabel("Success rate")
plt.suptitle("Success rate of recovering e by scalar product")
plt.title(r"with k=n/2 and t=$\sqrt{n}$")
plt.legend()
plt.show()
