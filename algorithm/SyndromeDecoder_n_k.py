import numpy as np
import numpy.ma as ma

from utils import SolutionTester


class SyndromeDecoder_N_K:
    def __init__(self, H, e, t):
        self.H = H
        self.e = e
        self.t = t
        self.s = self.H.dot(self.e)
        self.matrix_lines = len(H)
        self.matrix_columns = len(H[0])
        self.H_transposed = np.transpose(self.H)

        phi = self.compute_phi()
        self.pi, self.e_prime, permutation = self.get_pi(phi)
        self.A_inverse = self.get_A_inverse()

        self.result = self.compute_result_chances()

    def compute_phi(self):
        matrix = ma.where(self.H_transposed == 0, self.t - self.s, self.s)
        sum = np.sum(matrix, axis=1)
        return sum

    def get_H_complementary(self):
        H_ones = np.ones_like(self.H_transposed)
        return H_ones - self.H_transposed

    def get_s_complementary(self):
        s_of_ts = np.full_like(self.s, self.t)
        return s_of_ts - self.s

    def get_pi(self, f):
        # descending order
        permutation = np.argsort(-f)
        e_prime = np.take(self.e, permutation, axis=0)
        pi = np.take(self.H, permutation, axis=1)

        return pi, e_prime, permutation

    # attempts to reconstruct the permuted error vector
    def get_A_inverse_result(self, s):
        if self.A_inverse is not None:
            return np.round(self.A_inverse.dot(s))

    def get_A_inverse(self):
        A = self.pi[0:self.matrix_lines, 0:self.matrix_lines]
        det = np.linalg.det(A)
        if det != 0:
            return np.linalg.inv(A)

    # gets the count of 0 values in the first k positions
    def get_error_indices(self):
        sub_result = self.result[:self.matrix_lines]
        non_zeros = np.count_nonzero(sub_result)

        return self.t - non_zeros

    def compute_result_chances(self):
        result = self.get_A_inverse_result(self.s)
        if SolutionTester.are_results_binary_t_equal(result, self.t, self.matrix_columns):
            return 1
        return 0

    def get_result_chances(self):
        return self.result
