import numpy


def get_syndrome(H, e):
    s = H.dot(e)
    s = s[:, numpy.newaxis]
    return s


def compare_column_hamming_weight(sorted_matrix, element):
    element_first = sorted_matrix[:, element]
    element_second = sorted_matrix[:, element + 1]
    is_greater = numpy.sum(element_second) > numpy.sum(element_first)
    return is_greater


def check_syndrome(syndrome):
    for i in syndrome:
        if i < 0:
            return False
    if not any(syndrome):
        return False
    return True


def find_solution(matrix, syndrome):
    group_start = group_end = line = 0
    matrix_line_length = len(matrix[0])
    solution_columns = []
    while check_syndrome(syndrome) and group_end < matrix_line_length:
        while group_end < matrix_line_length and matrix[line, group_end]:
            group_end += 1
        for nr in range(syndrome[line][0]):
            if group_start < group_end:
                rand = numpy.random.randint(group_start, group_end)
                if rand not in solution_columns:
                    solution_columns.append(rand)
                    len_matrix = len(matrix)  # columns length
                    reshaped = matrix[:, rand].reshape(len_matrix, 1)
                    syndrome -= reshaped
        line += 1
        group_start = group_end

    return solution_columns


def get_filtered_indices(column_indices, current_line):
    filter_indices = []
    for index in column_indices:
        if index[1] > index[0] >= current_line:
            filter_indices.append(index)
    return filter_indices


class SyndromeDecoderOriginal(object):

    def __init__(self, H, e, t):
        self.H = H
        self.e = e
        self.t = t
        self.matrix_lines = len(H)
        self.matrix_columns = len(H[0])
        self.s = get_syndrome(self.H, self.e)
        self.s_ordered, H_prime = self.sort_syndrome()
        self.H_prime = self.sort_H_prime(H_prime)
        self.H_ordered, e_prime = self.sort_matrix()
        self.found_e = self.find_columns_combo(self.H_ordered, e_prime)
        self.syndrome = numpy.copy(self.s_ordered)
        count = 0
        while any(self.syndrome) and count < 2000000:
            self.syndrome = numpy.copy(self.s_ordered)
            self.solution = find_solution(self.H_ordered, self.syndrome)
            count += 1

        print("original solution ", self.solution)

    # sort the syndrome in a ascending manner and apply the same index modifications to the matrix
    # eg. swap line 3 in s with line 2 and do the same in the matrix
    # return s' and H'
    def sort_syndrome(self):
        s_prime = numpy.copy(self.s)
        H_prime = numpy.copy(self.H)
        needs_reordering = True
        syndrome_range = len(s_prime) - 1
        while needs_reordering:
            needs_reordering = False
            for element in range(syndrome_range):
                if s_prime[element] > s_prime[element + 1]:
                    first_s_element = numpy.copy(s_prime[element, :])
                    second_s_element = numpy.copy(s_prime[element + 1, :])
                    s_prime[element, :] = second_s_element
                    s_prime[element + 1, :] = first_s_element
                    H_prime[[element, element + 1], :] = H_prime[[element + 1, element], :]
                    needs_reordering = True
        return s_prime, H_prime

    def sort_matrix(self):
        sorted_matrix = numpy.copy(self.H_prime)
        e_prime = numpy.copy(self.e)
        matrix_range = len(sorted_matrix[0]) - 1
        for each in range(matrix_range):
            for element in range(matrix_range):
                if compare_column_hamming_weight(sorted_matrix, element):
                    first_element = numpy.copy(sorted_matrix[:, element])
                    second_element = numpy.copy(sorted_matrix[:, element + 1])
                    sorted_matrix[:, element] = second_element
                    sorted_matrix[:, element + 1] = first_element

                    first_e_element = numpy.copy(e_prime[element])
                    second_e_element = numpy.copy(e_prime[element + 1])
                    e_prime[element] = second_e_element
                    e_prime[element + 1] = first_e_element

        return sorted_matrix, e_prime

    def sort_H_prime(self, matrix):
        start_col = line = 0
        matrix_lines = len(matrix)
        matrix_columns = len(matrix[0])
        while line < matrix_lines and start_col < matrix_columns:
            matrix, start_col = self.sort_H_prime_lines(matrix, line, start_col)
            line += 1
        return matrix

    def sort_H_prime_lines(self, matrix, line, start_col):
        matrix_line_length = len(matrix[0]) - 1
        i = start_col
        last_one = start_col
        j = matrix_line_length
        while i < j:
            if not matrix[line, i]:
                while not matrix[line, j]:
                    j -= 1
                if j > i:
                    matrix[:, [i, j]] = matrix[:, [j, i]]
                    last_one = i + 1
            else:
                last_one = i + 1
            i += 1
        return matrix, last_one

    def find_columns_combo(self, matrix, vector):
        vector_length = len(vector)
        dict_e = {}
        for i in range(vector_length):
            sum_cols = matrix[:, i]
            sum_cols = sum_cols[:, numpy.newaxis]
            list_of_added_columns = [i]
            list_of_subtracted = []
            found_e= self.find_column_combo(matrix, vector, sum_cols, list_of_added_columns, list_of_subtracted)
            dict_e[i] = found_e
        return dict_e

    def find_column_combo(self, matrix, vector, sum_cols, list_of_added_columns, list_of_subtracted):
        found_e = numpy.zeros(len(matrix[0]), dtype=int)
        for index in range(len(vector)):
            if index in list_of_added_columns or index in list_of_subtracted:
                continue
            elif any(sum_cols < self.s_ordered):
                sum_cols[:, 0] += matrix[:, index]
                if any(sum_cols > self.s_ordered):
                    sum_cols[:, 0] -= matrix[:, index]
                    list_of_subtracted.append(index)
                    continue
                else:
                    list_of_added_columns.append(index)
            elif all(sum_cols == self.s_ordered):
                for i in range(len(list_of_added_columns)):
                    found_e[list_of_added_columns[i]] = 1
                break
        return found_e

    def print_values(self):
        print("s \n", self.s)
        print("s ordered \n", self.s_ordered)
        print("H prime \n", self.H_prime)
        print("H ordered \n", self.H_ordered)
        print("solution \n", self.solution)
