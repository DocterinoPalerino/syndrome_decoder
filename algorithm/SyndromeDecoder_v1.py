import numpy
import operator
import random


def get_syndrome(H, e):
    s = H.dot(e)
    s = s[:, numpy.newaxis]
    return s


# compares the hamming weight at the specified indices and returns if the second compared value is greater
def compare_column_hamming_weight(sorted_matrix, index):
    first_column = sorted_matrix[:, index]
    second_column = sorted_matrix[:, index + 1]
    is_greater = numpy.sum(second_column) > numpy.sum(first_column)
    return is_greater


# Filter indices based on 2 rules:
# 1. index 1 must be greater than index 0
# 2. both indices should be greater than the current line the values selected from
def get_filtered_indices(column_indices, current_line):
    filter_indices = []
    for index in column_indices:
        index_0 = index[0] + current_line
        index_1 = index[1] + current_line
        if index_1 > index_0 >= current_line:
            new_values = (index_0, index_1)
            filter_indices.append(new_values)
    return filter_indices


class SyndromeDecoderV1(object):

    def __init__(self, H, e, t):
        self.H = H
        self.e = e
        self.t = t
        self.matrix_lines = len(H)
        self.matrix_columns = len(H[0])
        self.s = get_syndrome(self.H, self.e)
        self.s_ordered, H_prime = self.sort_syndrome_ascending(self.s, self.H)
        self.H_prime = self.sort_H_columns_by_1_occurrence(H_prime)
        self.H_ordered_HW = self.sort_matrix_columns_by_HW(self.H_prime, self.e)
        syndrome = numpy.copy(self.s_ordered)
        self.syndrome = numpy.squeeze(syndrome)
        count = 0
        while numpy.any(self.syndrome) and count < 2000000:
            syndrome = numpy.copy(self.s_ordered)
            self.syndrome = numpy.squeeze(syndrome)
            self.solution = self.find_solution(self.H_ordered_HW)
            count += 1

        print("my solution ", self.solution)

    # sort the syndrome in a ascending manner and apply the same index modifications to the matrix
    # eg. swap line 3 in s with line 2 and do the same in the matrix
    # return s' and H'
    def sort_syndrome_ascending(self, s, H):
        index_values_dict = {}
        for i in range(len(s)):
            index_values_dict[i] = int(s[i])
        ordered_dict = dict(sorted(index_values_dict.items(), key=operator.itemgetter(1)))
        s_prime = numpy.zeros(shape=(len(s), 1), dtype=int)
        H_prime = numpy.zeros(shape=(self.matrix_lines, self.matrix_columns), dtype=int)
        keys_list = list(ordered_dict)
        keys_list_range = len(keys_list)
        for i in range(keys_list_range):
            ordered_s_index = keys_list[i]
            s_prime[i] = s[ordered_s_index]
            H_prime[i] = H[ordered_s_index]
        return s_prime, H_prime

    # sort H prime by swapping the first column which contains 0 with the last column that contains 1
    # do this for every subsequent column, ensuring that the ordering of the previous swapped columns is kept,
    # for this use a current column variable that tracks where the last 0 was flipped
    # eg: in 1 1 0 1 0 1 flip index 2 with 5 and set current column to index 2,
    # subsequent searches for 0 and and 1 should start at that index
    def sort_H_columns_by_1_occurrence(self, matrix):
        current_column = 0
        for line in range(self.matrix_lines):
            matrix_line = matrix[line][current_column:]
            matrix_to_search_0 = numpy.where(matrix_line == 0)[0]
            matrix_to_search_1 = numpy.where(matrix_line == 1)[0]
            matrix_to_search_1_flipped = tuple(reversed(matrix_to_search_1))
            # form pairs of indices of found 0 and 1
            column_indices = list(zip(matrix_to_search_0, matrix_to_search_1_flipped))
            filtered_indices = get_filtered_indices(column_indices, current_column)
            # swap columns
            for value in filtered_indices:
                matrix[:, [value[0], value[1]]] = matrix[:, [value[1], value[0]]]
            # set the position at which columns should start swapping
            if len(filtered_indices) > 0:
                first_0_position = numpy.where(matrix_line == 0)[0][0]
                last_swap_position = filtered_indices[0][0] + 1
                current_column = max(last_swap_position, first_0_position)
        return matrix

    # sort the matrix columns based on Hamming Weight. Columns with higher HM are put the left.
    # Order the e vector the same way using the previous results.
    # e.g: column 3 has a HM of 6 and column 2 a HM of 3, then swap these 2 column
    # swap the same columns in the error vector
    def sort_matrix_columns_by_HW(self, H, e):
        column_indices_dict = {}
        for i in range(self.matrix_columns):
            column_indices_dict[i] = numpy.sum(H[:, i])
        ordered_dict = dict(sorted(column_indices_dict.items(), key=operator.itemgetter(1), reverse=True))
        HM_matrix = numpy.zeros(shape=(self.matrix_lines, self.matrix_columns), dtype=int)
        keys = list(ordered_dict.keys())
        for i in range(len(keys)):
            value = keys[i]
            HM_matrix[:, i] = H[:, value]
        return HM_matrix

    # get random unique nrs between start and end range(last positive nr) repeated syndrome range times
    # take the column at index value generated from this random sample
    # subtract this random column from syndrome
    # repeat until syndrome is 0
    # when the syndrome is 0 return solutions
    def find_solution(self, matrix):
        start = line = 0
        solution_columns = []
        while numpy.all(self.syndrome > 0):
            end = self.get_last_non_zero_index(matrix[line])
            if start < end:
                syndrome_range = self.syndrome[line]
                min_range = min(syndrome_range, end)
                solution_columns = random.sample(range(start, end), min_range)
                for random_nr in solution_columns:
                    matrix_column = matrix[:, random_nr]
                    self.syndrome -= matrix_column
            line += 1
            start = end
        return solution_columns

    def print_values(self):
        print("s \n", self.s)
        print("s ordered \n", self.s_ordered)
        print("H prime \n", self.H_prime)
        print("H ordered \n", self.H_ordered_HW)
        print("solution \n", self.solution)

    def get_last_non_zero_index(self, matrix_line):
        non_zero_indices = numpy.nonzero(matrix_line)[0]
        return non_zero_indices[len(non_zero_indices) - 1]
