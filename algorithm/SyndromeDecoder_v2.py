import itertools
import operator

import numpy as np


# Filter indices based on 2 rules:
# 1. index 1 must be greater than index 0
# 2. both indices should be greater than the current line the values selected from
def get_filtered_indices(column_indices, current_column):
    filter_indices = []
    for index in column_indices:
        index_0 = index[0] + current_column
        index_1 = index[1] + current_column
        if index_1 > index_0 >= current_column:
            new_values = (index_0, index_1)
            filter_indices.append(new_values)
    return filter_indices


class SyndromeDecoderV2:
    def __init__(self, H, e, t):
        self.H = H
        self.e = e
        self.t = t
        self.matrix_lines = len(H)
        self.matrix_columns = len(H[0])
        self.s = self.H.dot(self.e)
        self.s_ordered, self.H_prime = self.sort_syndrome_ascending(self.s, self.H)
        self.H_ones_left = self.sort_H_columns_by_1_occurrence(self.H_prime)
        self.support = self.generate_support()
        self.support_line_length = len(self.support)
        self.final_solutions = []
        self.generate_solutions()
        self.check_solutions()

    # sort the syndrome in a ascending manner and apply the same index modifications to the matrix
    # eg. swap line 3 in s with line 2 and do the same in the matrix
    # return s' and H'
    def sort_syndrome_ascending(self, s, H):
        index_values_dict = {}
        for i in range(len(s)):
            index_values_dict[i] = int(s[i])
        ordered_dict = dict(sorted(index_values_dict.items(), key=operator.itemgetter(1)))
        s_prime = np.zeros(shape=(len(s)), dtype=int)
        H_prime = np.zeros(shape=(self.matrix_lines, self.matrix_columns), dtype=int)
        keys_list = list(ordered_dict)
        keys_list_range = len(keys_list)
        for i in range(keys_list_range):
            ordered_s_index = keys_list[i]
            s_prime[i] = s[ordered_s_index]
            H_prime[i] = H[ordered_s_index]
        return s_prime, H_prime

    # sort H prime by swapping the first column which contains 0 with the last column that contains 1
    # do this for every subsequent column, ensuring that the ordering of the previous swapped columns is kept,
    # for this use a current column variable that tracks where the last 0 was flipped
    # eg: in 1 1 0 1 0 1 flip index 2 with 5 and set current column to index 2,
    # subsequent searches for 0 and and 1 should start at that index
    def sort_H_columns_by_1_occurrence(self, matrix):
        current_column = first_0_position = last_swap_position = 0
        for line in range(self.matrix_lines):
            matrix_line = matrix[line][current_column:]
            matrix_to_search_0 = np.where(matrix_line == 0)[0]
            matrix_to_search_1 = np.where(matrix_line == 1)[0]
            matrix_to_search_1_flipped = tuple(reversed(matrix_to_search_1))
            # form pairs of indices of found 0 and 1
            column_indices = list(zip(matrix_to_search_0, matrix_to_search_1_flipped))
            filtered_indices = get_filtered_indices(column_indices, current_column)
            # swap columns
            for value in filtered_indices:
                matrix[:, [value[0], value[1]]] = matrix[:, [value[1], value[0]]]
            # set the position at which columns should start swapping
            positions_0 = np.where(matrix_line == 0)[0]
            if positions_0.size != 0:
                first_0_position = positions_0[0] + current_column
            if len(filtered_indices) != 0:
                # the first 0 index the column was swapped
                last_swap_position = filtered_indices[0][0] + 1
            current_column = max(last_swap_position, first_0_position)
        return matrix

    # Generates the indices of 1 on each line where the stop flag index is put at the first 0 occurrence or the end
    # of a line. Then the position of this flag reflects the start of the search of the next 1 indices on the 2nd
    # line, this reiterates until all lines or the column index are exhausted. Because every iteration the nr. of
    # columns are sliced, the range generated needs to be adjusted
    def generate_support(self):
        support_list = []
        last_nonzero_index = line = 0
        while last_nonzero_index < self.matrix_columns and line < self.matrix_lines:
            matrix_line = self.H_ones_left[line, last_nonzero_index:]
            positions_1 = np.where(matrix_line == 1)[0]
            positions_0 = np.where(matrix_line == 0)[0]
            # were any 0-s found?
            if positions_0.size == 0:
                position_0 = len(matrix_line)
            else:
                position_0 = positions_0[0]

            # were any 1-s found?
            if positions_1.size != 0:
                position_1 = positions_1[0]
                range_nrs = range(position_1 + last_nonzero_index, position_0 + last_nonzero_index)
                support_list.append(list(range_nrs))
                last_nonzero_index = position_0 + last_nonzero_index
            else:
                support_list.append([])
            line += 1
        return support_list

    # ensure that there is at least one valid support line in this line or the next lines
    def check_support(self, line):
        for i in range(line, len(self.support)):
            if len(self.support[i]):
                return True
        return False

    # generate solutions:
    # 1. precompute all possible combinations from the max syndrome value and going backwards
    # 2. attempt to build solutions from given syndrome
    def generate_solutions(self):
        syndrome = self.s_ordered
        max_syndrome_value = syndrome.max()
        self.combinations_dict = self.get_combination_dict(max_syndrome_value)
        solution = [[], syndrome]
        line = 0
        self.get_new_solution_from_combinations(solution, line)

    # compute the solutions for possible combinations such that the syndrome becomes 0
    # if a syndrome is found to have at least 1 element greater than 0,
    # then this solution is passed recursively to another iteration
    # if a syndrome is all 0 then a final solution is appended
    # if a syndrome has elements lesser than 0, then this solution is dropped and the iteration returns to the original
    # combination
    # eg. : solution 0: [0, 123]
    # solution 1: [0 2, 100]
    # solution 2 [0 2 4, -100] - failed solution, go to next iteration or back to solution 1
    def get_new_solution_from_combinations(self, solution, line):
        syndrome = solution[1]
        new_line, combinations = self.get_valid_combinations(line, syndrome)
        if combinations is None:
            return

        for combination in combinations:
            syndrome = self.get_subtracted_syndrome(combination, syndrome)
            syndrome_valid = np.all(syndrome >= 0)
            # does the syndrome still have positive numbers and we still have valid support lines?
            if syndrome_valid and self.check_support(line):
                self.get_new_solution_from_combinations([solution[0] + list(combination), syndrome], new_line)
            all_0 = np.count_nonzero(syndrome) == 0
            if all_0:
                self.final_solutions.append([solution[0] + list(combination), syndrome])
            syndrome = solution[1]

    # get a list of valid combinations using the line as as index for the support. The support will act as a pool for
    # possible combinations, while the syndrome value at line dictates the length of the combination.
    # Ensures that the combination returned is at the next valid line, thus updating the line as well
    def get_valid_combinations(self, line, syndrome):
        combinations = []
        # make sure we're not dealing with an empty combination set
        # and we're not going outside the bounds of the line support
        while (len(combinations) == 0 or len(combinations[0]) == 0) and self.check_support(line):
            syndrome_value = syndrome[line]
            combinations = self.combinations_dict[syndrome_value][line]
            line += 1
        if len(combinations) == 0 or len(combinations[0]) == 0:
            return line - 1, None
        return line - 1, combinations

    # subtract values from syndrome with the column in H at the combination index
    def get_subtracted_syndrome(self, combination, syndrome):
        for column_index in combination:
            col = self.H_ones_left[:, column_index]
            syndrome = syndrome - col
        return syndrome

    # gets the set of precomputed set of combinations from max syndrome value to 0 for each support line
    def get_combination_dict(self, max_syndrome_value):
        syndrome_values_dict = {}
        for syndrome_value in range(max_syndrome_value):
            support_dict = {}
            syndrome_values_dict[syndrome_value] = support_dict
            for line in range(self.support_line_length):
                value = tuple(itertools.combinations(self.support[line], syndrome_value))
                support_dict[line] = value

        return syndrome_values_dict

    def check_solutions(self):
        my_solutions = self.final_solutions[0][0]
        nr_solutions = len(my_solutions)
        for each_column in range(nr_solutions):
            solution = my_solutions[each_column]
            self.s_ordered -= self.H_ones_left[:, solution]
        print(self.s_ordered)

        # nr_solutions = len(my_solutions)
        # nr_lines = len(self.H)
        # matrix = np.zeros((nr_solutions, nr_lines), dtype=int)
        # for each in range(nr_solutions):
        #     solution = my_solutions[each]
        #     matrix[each] = self.H_ones_left[:, solution]
        #
        # matrix_t = np.transpose(matrix)
        # det = np.linalg.det(matrix_t)
        # if det != 0:
        #     matrix_inv = np.linalg.inv(matrix_t)
        #     result = np.round(matrix_inv.dot(self.s))
        #     print(result)
        # else:
        #     print("failed")
