import numpy as np

import numpy as np

from database import Database_sd
from utils import algorithm_names


def get_psi_result(H, e, t, H_transposed, H_complementary):
    s = H.dot(e)
    perm_psi = calculate_psi_permutation(H_complementary, H_transposed, s, t)
    e_prime_psi, permutation_psi = compute_permutation(perm_psi, e)
    return e_prime_psi, permutation_psi


def calculate_psi_permutation(H_complementary, H_transposed, s, t):
    s_c = get_s_complementary(s, t)
    phi = H_transposed.dot(s)
    phi_c = H_complementary.dot(s_c)
    return phi + phi_c


def get_s_complementary(s, t):
    s_of_ts = np.full_like(s, t)
    return s_of_ts - s


def compute_permutation(permutation_list, e):
    # descending order
    permutation = np.argsort(-permutation_list)
    e_prime = np.take(e, permutation, axis=0)

    return e_prime, permutation


def get_results(algorithm, H, e, t, H_transposed, H_complementary):
    if algorithm == algorithm_names.PSI:
        return get_psi_result(H, e, t, H_transposed, H_complementary)
    return None
