import numpy as np
import itertools


def get_syndrome(H, e):
    s = H.dot(e)
    s = s[:, np.newaxis]
    return s


def filter_solution(solution):
    syndrome = np.squeeze(solution[1])
    return np.count_nonzero(syndrome) == 0


def filter_solutions(solutions):
    new_solutions = list(filter(filter_solution, solutions))
    return new_solutions


class SyndromeDecoderOriginalV2:
    def __init__(self, H, e, t):
        self.H = H
        self.e = e
        self.t = t
        self.matrix_lines = len(H)
        self.matrix_columns = len(H[0])
        self.s = get_syndrome(self.H, self.e)
        self.s_ordered, self.H_prime = self.sort_syndrome()
        line = start_col = 0
        H_prime = np.copy(self.H_prime)
        while line < self.matrix_lines and start_col < self.matrix_columns - 1:
            self.H_second, start_col = self.sort_matrix(H_prime, line, start_col)
            line += 1
        self.L = self.generate_L(self.H_second)
        self.solutions, self.number_of_solutions, self.cuted_solutions = self.generates_solution(self.H_second,
                                                                                                 self.s_ordered,
                                                                                                 self.L)
        self.solutions = filter_solutions(self.solutions)

    @staticmethod
    def sort_matrix(sorted_matrix, line, start_col):
        n = len(sorted_matrix[1]) - 1
        i = start_col
        j = n
        while i < j:
            if not sorted_matrix[line, i]:
                while not sorted_matrix[line, j]:
                    j = j - 1
                if j > i:
                    sorted_matrix[:, [i, j]] = sorted_matrix[:, [j, i]]
                    start_col = i + 1
            else:
                start_col = i + 1
            i += 1
        return sorted_matrix, start_col

    # SAME as code in original
    def sort_syndrome(self):
        sind = np.copy(self.s)
        matrix = np.copy(self.H)
        sorted_sind = sind
        permuted_matrix = matrix
        counter = len(sind) - 1
        while counter:
            for i in range(len(sorted_sind) - 1):
                if sorted_sind[i] > sorted_sind[i + 1]:
                    sorted_sind[[i], :], sorted_sind[[i + 1], :] = sorted_sind[[i + 1], :], sorted_sind[[i], :]
                    permuted_matrix[[i, i + 1], :] = permuted_matrix[[i + 1, i], :]
            counter -= 1
        return sorted_sind, permuted_matrix

    @staticmethod
    def check_syndrome(syndrome):
        for i in syndrome:
            if i < 0:
                return False
        return True

    @staticmethod
    def update_syndrome(synd, col):
        return synd - col

    @staticmethod
    def generate_L(matrix):
        L = list()
        li = list()
        line = end_grup = 0
        while line < len(matrix):
            li = []
            while end_grup < len(matrix[0]) and matrix[line, end_grup]:
                li.append(end_grup)
                end_grup += 1
            line += 1
            L.append(li)
        return L

    @staticmethod
    def generate_subsets(elem, n):
        return list(itertools.combinations(elem, n))

    @staticmethod
    def check_L(L, line):
        for i in range(line, len(L)):
            if len(L[i]):
                return True
        return False

    def generates_solution(self, matrix, synd, L):
        solutions = list()
        number_of_solutions = list()
        number_of_cuted_solutions = list()
        subsets = self.generate_subsets(L[0], synd[0][0])
        for subset in subsets:
            new_synd = synd
            for i in subset:
                col = matrix[:, i].reshape(len(synd), 1)
                new_synd = self.update_syndrome(new_synd, col)
            solutions.append([list(subset), new_synd])
        number_of_solutions.append(len(solutions))
        line = 1
        while self.check_L(L, line):
            subsets = dict()
            new_solutions = list()
            cuted_sols = 0
            for solution in solutions:
                sol = solution[0]
                new_synd = solution[1]
                if new_synd[line][0] not in subsets.keys():
                    subsets.update({new_synd[line][0]: self.generate_subsets(L[line], new_synd[line][0])})
                for subset in subsets[new_synd[line][0]]:
                    for i in subset:
                        col = matrix[:, i].reshape(len(synd), 1)
                        new_synd = self.update_syndrome(new_synd, col)
                    if self.check_syndrome(new_synd):
                        new_solutions.append([sol + list(subset), new_synd])
                    new_synd = solution[1]

            line += 1
            if len(solutions) > len(new_solutions):
                number_of_cuted_solutions.append(len(solutions) - len(new_solutions) + cuted_sols)
            else:
                number_of_cuted_solutions.append(0)
            number_of_solutions.append(len(new_solutions))
            solutions = new_solutions

        return solutions, number_of_solutions, number_of_cuted_solutions
