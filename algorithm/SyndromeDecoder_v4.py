import numpy as np


class SyndromeDecoderV4:

    def __init__(self, H, e, t):
        self.H = H
        self.e = e
        self.t = t
        self.matrix_lines = len(H)
        self.matrix_columns = len(H[0])
        self.H_transposed = np.transpose(self.H)
        self.s = self.H.dot(self.e)
        self.f = self.calculate_f(self.get_H_complementary())
        self.e_prime, self.permutation = self.get_phi()

    def __init__(self, H, e, t, H_transposed, H_complementary):
        self.H = H
        self.e = e
        self.t = t
        self.H_transposed = H_transposed
        self.matrix_lines = len(H)
        self.matrix_columns = len(H[0])
        self.s = self.H.dot(self.e)
        self.f = self.calculate_f(H_complementary)
        self.e_prime, self.permutation = self.get_phi()

    def get_H_complementary(self):
        H_ones = np.ones_like(self.H_transposed)
        return H_ones - self.H_transposed

    def calculate_f(self, H_complementary):
        s_c = self.get_s_complementary()
        phi = self.H_transposed.dot(self.s)
        phi_c = H_complementary.dot(s_c)
        return phi + phi_c

    def get_s_complementary(self):
        s_of_ts = np.full_like(self.s, self.t)
        return s_of_ts - self.s

    def get_phi(self):
        # descending order
        permutation = np.argsort(-self.f)
        e_prime = np.take(self.e, permutation, axis=0)

        return e_prime, permutation

    # gets the count of 0 values in the first k positions
    def get_error_indices(self):
        sub_result = self.e_prime[:self.matrix_lines]
        non_zeros = np.count_nonzero(sub_result)

        return self.t - non_zeros
