import numpy as np
import numpy.ma as ma


class SyndromeDecoder_T:
    def __init__(self, H, e, t):
        self.H = H
        self.e = e
        self.t = t
        self.s = self.H.dot(self.e)
        self.matrix_lines = len(H)
        self.matrix_columns = len(H[0])
        self.H_transposed = np.transpose(self.H)

        phi = self.compute_phi()
        self.pi, self.e_prime, permutation = self.get_pi(phi)
        self.result = self.compute_result_chances()

    def compute_phi(self):
        matrix = ma.where(self.H_transposed == 0, self.t - self.s, self.s)
        sum = np.sum(matrix, axis=1)
        return sum

    def get_H_complementary(self):
        H_ones = np.ones_like(self.H_transposed)
        return H_ones - self.H_transposed

    def get_s_complementary(self):
        s_of_ts = np.full_like(self.s, self.t)
        return s_of_ts - self.s

    def get_pi(self, f):
        # descending order
        permutation = np.argsort(-f)
        e_prime = np.take(self.e, permutation, axis=0)
        pi = np.take(self.H, permutation, axis=1)

        return pi, e_prime, permutation

    def compute_result_chances(self):
        e_prime = np.zeros(self.matrix_columns).astype(int)
        e_prime[:self.t] = 1
        if np.array_equal(self.pi.dot(e_prime), self.s):
            return 1
        return 0

    def get_result_chances(self):
        return self.result
