import numpy as np

from database import Database_sd
from utils import algorithm_names


class SyndromeDecoder_v5:

    def __init__(self, H, e, t, H_transposed, H_complementary):
        self.H = H
        self.e = e
        self.t = t
        self.H_transposed = H_transposed
        self.matrix_lines = len(H)
        self.matrix_columns = len(H[0])
        self.s = self.H.dot(self.e)
        self.perm_psi = self.calculate_psi_permutation(H_complementary)
        # self.perm_phi = self.calculate_phi_permutation()
        self.perm_eucl_psi, self.perm_eucl_psi_ord_5 = self.calculate_euclidian_permutation(H_complementary)
        # self.subtract_score = self.calculate_subtract_permutation()

        # self.e_prime_phi, self.permutation_phi = self.compute_permutation(self.perm_phi)
        self.e_prime_psi, self.permutation_psi = self.compute_permutation(self.perm_psi)
        self.e_euclidian_psi, self.permutation_eucl_psi = self.compute_permutation(self.perm_eucl_psi)
        # self.e_euclidian_phi, self.permutation_eucl_phi = self.compute_permutation(self.perm_eucl_phi)
        # self.e_euclidian_phi_ord_5, self.permutation_eucl_phi_ord_5 = self.compute_permutation(self.perm_eucl_phi_ord_5)
        self.e_euclidian_psi_ord_5, self.permutation_eucl_psi_ord_5 = self.compute_permutation(self.perm_eucl_psi_ord_5)
        # self.e_subtract_score, self.permutation_subtract_score = self.compute_permutation(self.subtract_score)

    def calculate_psi_permutation(self, H_complementary):
        s_c = self.get_s_complementary()
        phi = self.H_transposed.dot(self.s)
        phi_c = H_complementary.dot(s_c)
        return phi + phi_c

    def calculate_euclidian_permutation(self, H_complementary):
        s_c = self.get_s_complementary()
        distances = [np.linalg.norm(each - self.s, ord=2) for each in self.H_transposed]
        distances_c = [np.linalg.norm(each - s_c, ord=2) for each in H_complementary]

        distances_ord_5 = [np.linalg.norm(each - self.s, ord=5) for each in self.H_transposed]
        distances_c_ord_5 = [np.linalg.norm(each - s_c, ord=5) for each in H_complementary]

        return np.add(distances, distances_c), np.add(distances_ord_5, distances_c_ord_5)

    def calculate_phi_permutation(self):
        return self.H_transposed.dot(self.s)

    def calculate_subtract_permutation(self):
        result = self.s - self.H_transposed
        return np.array([np.sum(line) for line in result])

    def get_s_complementary(self):
        s_of_ts = np.full_like(self.s, self.t)
        return s_of_ts - self.s

    def compute_permutation(self, permutation_list):
        # descending order
        permutation = np.argsort(-permutation_list)
        e_prime = np.take(self.e, permutation, axis=0)

        return e_prime, permutation

    def get_psi_result(self):
        return self.e_prime_psi, self.permutation_psi

    def get_phi_result(self):
        return self.e_prime_phi, self.permutation_phi

    def get_phi_eucl_result(self):
        return self.e_euclidian_phi, self.permutation_eucl_phi

    def get_psi_eucl_result(self):
        return self.e_euclidian_psi, self.permutation_eucl_psi

    def get_subtract_result(self):
        return self.e_subtract_score, self.permutation_subtract_score

    def get_phi_eucl_result_ord_5(self):
        return self.e_euclidian_phi_ord_5, self.permutation_eucl_phi_ord_5

    def get_psi_eucl_result_ord_5(self):
        return self.e_euclidian_psi_ord_5, self.permutation_eucl_psi_ord_5

    def get_results(self, algorithm):
        if algorithm == algorithm_names.PSI:
            return self.get_psi_result()
        elif algorithm == algorithm_names.PSI_EUCL_ORD_5:
            return self.get_psi_eucl_result_ord_5()
        elif algorithm == algorithm_names.PSI_EUCL_ORD_2:
            return self.get_psi_eucl_result()
        return None
