from utils import SyndromeDecoderSample
import numpy as np

from algorithm.SyndromeDecoder_v3 import SyndromeDecoderV3


K_RANGE = range(300, 1100, 100)
TESTS = 1000


# generates a matrix for each k and a static N and T
class TestRunner:

    def __init__(self, n, t):
        self.rng = np.random.default_rng()
        self.n = n
        self.t = t
        L = self.run_test_range_k()
        print(L)

    def run_test_range_k(self):
        L = []
        for k in K_RANGE:
            matrix = SyndromeDecoderSample.get_random_matrix(self.n, k)
            t_sub_dict = {}
            error_vectors = self.get_error_vectors(self.t, self.n)
            results = []
            for e in error_vectors:
                decoder_v3 = SyndromeDecoderV3(matrix, e, self.t)
                result = decoder_v3.get_error_indices()
                results.append(result)
            succes_chance_results = self.get_chance_results(results)
            final_results = np.average(succes_chance_results)
            t_sub_dict[k] = final_results
            L.append(t_sub_dict)
            print(L)
        return L

    def get_error_vectors(self, t, n):
        base_error_vector = np.zeros(n).astype(int)
        base_error_vector[:t] = 1
        error_vectors = [
            SyndromeDecoderSample.get_error_vector_v2(base_error_vector, self.rng) for i in range(TESTS)]
        return error_vectors

    def get_chance_results(self, results):
        results = np.array(results)
        chance_results = np.where(results == 0, 1, 0)
        return chance_results
