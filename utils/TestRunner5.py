import itertools
import math
import time

import numpy as np

from algorithm.SyndromeDecoder_n_k import SyndromeDecoder_N_K
from algorithm.SyndromeDecoder_n_k_exhaustive import SyndromeDecoder_N_K_Exhaustive
from algorithm.SyndromeDecoder_t import SyndromeDecoder_T
from database.Database_sd import T_ALG, N_K_ALG, EXH_SEARCH_ALG
from utils import SyndromeDecoderSample

N_ONE = 2000
N_TWO = 10000
RANGE_T_ONE = range(round(math.sqrt(N_ONE * math.log(N_ONE, 10))), round(math.sqrt(N_ONE)), -1)
RANGE_T_TWO = range(round(math.sqrt(N_TWO * math.log(N_TWO, 10))), round(math.sqrt(N_TWO)), -1)

P_RANGE = range(3, 0, -1)

MATRIX_PARAM_ONE = (N_ONE, 700, range(50, 750, 50), RANGE_T_ONE)
MATRIX_PARAM_ONE_EXH = (N_ONE, 700, range(250, 750, 50), RANGE_T_ONE)

MATRIX_PARAM_TWO = (N_TWO, 3000, range(100, 3100, 100), RANGE_T_TWO)
TEST_VECTORS = 1000
PARAMS = (MATRIX_PARAM_ONE, MATRIX_PARAM_TWO)


def run_t_alg(rng, matrix, params, database):
    range_k = params[2]
    range_t = params[3]
    n = params[0]

    for k in range_k:
        for t in range_t:
            results, time_results = [], []
            success_results_of_one = 0
            error_vectors = get_error_vectors(rng, n, t)
            sub_matrix = matrix[:k]
            for e in error_vectors:
                start = time.process_time()
                t_alg = SyndromeDecoder_T(sub_matrix, e, t)
                end = time.process_time() - start
                results.append(t_alg.get_result_chances())
                time_results.append(end)
            final_results = np.average(results)
            final_time_results = np.average(time_results)
            database.insert_to_db(T_ALG, n, k, t, final_results, final_time_results)
            # stop running this test if we have 3 consequent results of 1
            if final_results == 1:
                success_results_of_one += 1
            if success_results_of_one == 3:
                break


def run_n_k_alg(rng, matrix, params, database):
    range_k = params[2]
    range_t = params[3]
    n = params[0]

    for k in range_k:
        for t in range_t:
            success_results_of_one = 0
            results, time_results = [], []
            error_vectors = get_error_vectors(rng, n, t)
            sub_matrix = matrix[:k]
            for e in error_vectors:
                start = time.process_time()
                n_k_alg = SyndromeDecoder_N_K(sub_matrix, e, t)
                end = time.process_time() - start
                results.append(n_k_alg.get_result_chances())
                time_results.append(end)
            final_results = np.average(results)
            final_time_results = np.average(time_results)
            print(n, k, t, final_results, final_time_results)
            database.insert_to_db(N_K_ALG, n, k, t, final_results, final_time_results)
            # stop running this test if we have 3 consequent results of 1
            if final_results == 1:
                success_results_of_one += 1
            if success_results_of_one == 3:
                break


def run_exh_alg(rng, matrix, params, database):
    range_k = params[2]
    range_t = params[3]
    n = params[0]

    for k in range_k:
        t_average_values = []
        for t in range_t:
            t_success_values = []
            for p in P_RANGE:
                combinations = set(itertools.combinations(range(n - k), p))
                results, time_results = [], []
                error_vectors = get_error_vectors(rng, n, t)
                sub_matrix = matrix[:k]
                for e in error_vectors:
                    start = time.process_time()
                    exh_alg = SyndromeDecoder_N_K_Exhaustive(sub_matrix, e, t, p, combinations)
                    end = time.process_time() - start
                    results.append(exh_alg.get_result_chances())
                    time_results.append(end)
                final_results = np.average(results)
                t_success_values.append(final_results)
                final_time_results = np.average(time_results)
                print(n, k, t, p, final_results, final_time_results)
                database.insert_to_db_exh(EXH_SEARCH_ALG, n, k, t, p, final_results, final_time_results)
            # check if 3 t-s have values of 1 in a row
            t_average_values.append(np.average(t_success_values))
            if np.average(t_average_values) == 1 and len(t_average_values) == 3:
                break
            elif len(t_average_values) > 3:
                t_average_values.clear()
                t_average_values.append(np.average(t_success_values))


def prepare_tests_one(database):
    n = MATRIX_PARAM_ONE[0]
    k = MATRIX_PARAM_ONE[1]

    rng = np.random.default_rng()
    matrix = SyndromeDecoderSample.get_random_matrix(n, k)

    run_t_alg(rng, matrix, MATRIX_PARAM_ONE, database)
    run_n_k_alg(rng, matrix, MATRIX_PARAM_ONE, database)
    run_exh_alg(rng, matrix, MATRIX_PARAM_ONE_EXH, database)


def prepare_tests_two(database):
    n = MATRIX_PARAM_TWO[0]
    k = MATRIX_PARAM_TWO[1]

    rng = np.random.default_rng()
    matrix = SyndromeDecoderSample.get_random_matrix(n, k)

    run_t_alg(rng, matrix, MATRIX_PARAM_TWO, database)
    run_n_k_alg(rng, matrix, MATRIX_PARAM_TWO, database)
    run_exh_alg(rng, matrix, MATRIX_PARAM_TWO, database)


def prepare_tests(database):
    prepare_tests_one(database)
    prepare_tests_two(database)


def get_error_vectors(rng, n, t):
    base_error_vector = np.zeros(n).astype(int)
    base_error_vector[:t] = 1
    error_vectors = [
        SyndromeDecoderSample.get_error_vector_v2(base_error_vector, rng) for i in range(TEST_VECTORS)]
    return error_vectors
