import time

from utils import SyndromeDecoderSample
import numpy as np

from algorithm.SyndromeDecoder_v3 import SyndromeDecoderV3
from algorithm.SyndromeDecoder_v5 import SyndromeDecoderV5
from algorithm.SyndromeDecoder_n_k import SyndromeDecoderV6

MAX_K = 4000
STEP = 100
K_RANGE = range(3000, MAX_K + STEP, STEP)
TESTS = 40


# generates static N, K, T matrix, where k is sliced in K_RANGE
class TestRunner_K_SLICE:

    def __init__(self, n, t):
        self.rng = np.random.default_rng()
        self.n = n
        self.t = t
        L = self.run_time_tests()
        print(L)

    def run_time_tests(self):
        L = {}
        matrix = SyndromeDecoderSample.get_random_matrix(self.n, MAX_K)

        for k in K_RANGE:
            sub_matrix = matrix[:k]
            error_vectors = self.get_error_vectors()
            time_results_v3, time_results_v6 = [], []
            for e in error_vectors:
                start = time.process_time()
                decoder_v3 = SyndromeDecoderV3(sub_matrix, e, self.t)
                end = time.process_time() - start
                time_results_v3.append(end)

                start = time.process_time()
                decoder_v6 = SyndromeDecoderV6(sub_matrix, e, self.t)
                end = time.process_time() - start
                if decoder_v3.get_result_chances() is not decoder_v6.get_result_chances():
                    print("not same")
                time_results_v6.append(end)
            L[k] = [np.average(time_results_v3), np.average(time_results_v6)]

        return L

    def run_tests(self):
        L = {}
        matrix = SyndromeDecoderSample.get_random_matrix(self.n, MAX_K)

        for k in K_RANGE:
            sub_matrix = matrix[:k]
            error_vectors = self.get_error_vectors()
            results, time_results = [], []
            instance = "v3"
            for e in error_vectors:
                start = time.process_time()
                if instance == "v5":
                    decoder_v5 = SyndromeDecoderV5(sub_matrix, e, self.t)
                    results.append(decoder_v5.get_result_chances())
                elif instance == "v3":
                    decoder_v3 = SyndromeDecoderV3(sub_matrix, e, self.t)
                    results.append(decoder_v3.get_result_chances())
                end = time.process_time() - start
                time_results.append(end)

            final_results = np.average(results)
            final_time_results = np.average(time_results)
            L[k] = [final_results, final_time_results]
            print(L)
        return L

    def get_error_vectors(self):
        base_error_vector = np.zeros(self.n).astype(int)
        base_error_vector[:self.t] = 1
        error_vectors = [
            SyndromeDecoderSample.get_error_vector_v2(base_error_vector, self.rng) for i in range(TESTS)]
        return error_vectors
