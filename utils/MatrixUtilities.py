import numpy


def get_H_complementary(matrix_transposed):
    H_ones = numpy.ones_like(matrix_transposed)
    return H_ones - matrix_transposed
