import multiprocessing
import traceback
from functools import partial

from database import Database_sd
from utils import SyndromeDecoderSample, algorithm_names, AlgorithmRunner, params


def prepare_nist_tests(database):
    file = 'nist_params.txt'
    extracted_params = []
    with open(file) as f:
        lines = f.readlines()
    for line in lines:
        numeric_values_str = line.split(', ')
        numeric_values = []
        for each in numeric_values_str:
            list = [int(s) for s in each.split() if s.isdigit()]
            numeric_values.append(list[0])
        extracted_params.append(numeric_values)

    nr_tests = params.TEST_VECTORS_THREE
    prepare_nist_alg(extracted_params, database, nr_tests)


def prepare_nist_alg(parameters, database, nr_tests):
    rng = SyndromeDecoderSample.get_rng()
    p = multiprocessing.Pool(4)
    algorithms = [algorithm_names.PSI]

    new_func = partial(run_nist_param_alg, rng, algorithms, nr_tests)
    final_results = p.map(new_func, parameters)

    for final_result in final_results:
        n = final_result[0]
        k = final_result[1]
        t = final_result[2]
        results = final_result[3]
        for algorithm, alg_results in results.items():
            if algorithm == algorithm_names.PSI:
                for p, p_result in alg_results.items():
                    database.insert_to_table_p(Database_sd.NIST_TABLE, algorithm, n, k, t, p, p_result)
            else:
                raise ValueError('no algorithm named ', algorithm)


def run_nist_param_alg(rng, algorithms, nr_tests, param):
    try:
        n = param[0]
        k = param[0] - param[1]
        t = param[2]
        matrix = SyndromeDecoderSample.get_random_matrix(n, k)

        results = AlgorithmRunner.run_algorithm_for_param(rng, matrix, algorithms, nr_tests, n, k, t)
        return n, k, t, results
    except Exception:
        traceback.print_exc()

