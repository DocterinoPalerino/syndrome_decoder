import numpy as np


# def test_sdp():
#     error_indices = get_error_indices()
#     if error_indices == 0:
#         indices_one = np.where(e_prime == 1)
#         matrix_to_add = np.take(pi, indices_one, axis=1)
#         sum = np.sum(matrix_to_add)
#         s_sum = np.sum(s)
#
#
# def test_matrices_equals_pi(A, B, e_prime_i, e_prime_ii):
#     are_same = np.array_equal(np.hstack((A, B)), pi)
#     is_s = np.array_equal(pi.dot(e_prime), s)
#     A_times_B = A.dot(e_prime_i) + B.dot(e_prime_ii)
#     is_correct_s = np.array_equal(A_times_B, s)
#
#
# def extra_data(self, result, s):
#     ones = np.where(result == 1)[0]
#     hw = self.t == len(ones)
#     value = self.pi.dot(result)
#     return np.array_equal(value, s), hw
#
#
def are_results_binary_t_equal(result, t, matrix_columns):
    if result is not None:
        v = np.zeros(matrix_columns, dtype=int)
        v[:len(result)] = result
        all_binary = np.logical_or(v == 0, v == 1).all() and np.count_nonzero(v) == t
        return all_binary
