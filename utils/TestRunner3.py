import numpy as np

from utils import SyndromeDecoderSample
from algorithm.SyndromeDecoder_v4 import SyndromeDecoderV4

T_RANGE = range(10, 500, 10)
TESTS = 1000

# generates a static matrix and TESTS amount of error vectors, where T is variable
class TestRunner3:

    def __init__(self, n, k):
        self.rng = np.random.default_rng()
        self.n = n
        self.k = k
        self.run_test_range_t()

    def run_test_range_t(self):
        self.matrix = SyndromeDecoderSample.get_random_matrix(self.n, int(self.n / 2))
        self.matrix_transposed = np.transpose(self.matrix)
        self.matrix_complementary = np.ones_like(self.matrix_transposed) - self.matrix_transposed
        t_sub_dict = self.get_results_for_matrix(self.n)

    def get_results_for_matrix(self, n):
        t_sub_dict = {}
        for each_t in T_RANGE:
            error_vectors = self.get_error_vectors(self.n)
            results = []
            for e in error_vectors:
                decoder_v4 = SyndromeDecoderV4(self.matrix, e, each_t, self.matrix_transposed,
                                               self.matrix_complementary)
                result = decoder_v4.get_error_indices()
                results.append(result)
            succes_chance_results = self.get_chance_results(results)
            final_results = np.average(succes_chance_results)
            t_sub_dict[each_t] = final_results
        return t_sub_dict

    def get_error_vectors(self, t):
        base_error_vector = np.zeros(self.n).astype(int)
        base_error_vector[:t] = 1
        error_vectors = [
            SyndromeDecoderSample.get_error_vector_v2(base_error_vector, self.rng) for i in range(TESTS)]
        return error_vectors

    def get_chance_results(self, results):
        results = np.array(results)
        chance_results = np.where(results == 0, 1, 0)
        return chance_results
