import multiprocessing
import traceback
from functools import partial

import numpy
import numpy as np

from algorithm.SyndromeDecoder_v5 import SyndromeDecoder_v5
from utils import SyndromeDecoderSample, algorithm_names, MatrixUtilities, params, AlgorithmRunner


def prepare_alg(database, params):
    n = params[0]
    k = params[1]
    range_k = params[2]
    range_t = params[3]

    seed = np.random.seed(99)
    rng = np.random.default_rng(seed)
    matrix = SyndromeDecoderSample.get_random_matrix(n, k)

    p = multiprocessing.Pool(4)
    algorithms = [algorithm_names.PSI, algorithm_names.PSI_EUCL_ORD_2, algorithm_names.PSI_EUCL_ORD_5]
    new_func = partial(run_alg, rng, matrix, algorithms, n, range_t)
    results = p.map(new_func, range_k)
    print(results)

    for result in results:
        k = result[1]
        algorithm_results = result[0]
        for t, algorithms_dict in algorithm_results.items():
            psi_result = algorithms_dict[algorithm_names.PSI][0]
            psi_eucl_2_result = algorithms_dict[algorithm_names.PSI_EUCL_ORD_2]
            psi_eucl_5_result = algorithms_dict[algorithm_names.PSI_EUCL_ORD_5]
            for p, p_result in algorithms_dict[algorithm_names.PSI][1].items():
                database.insert_to_psi_and_psi_eucl_p(n, k, t, p, p_result)

            database.insert_to_psi_and_psi_eucl(n, k, t, psi_result, psi_eucl_2_result, psi_eucl_5_result)


def run_alg(rng, matrix, algorithms, n, range_t, k):
    try:
        averages = {t: {algorithm: None for algorithm in algorithms} for t in range_t}
        p_averages = {p: None for p in params.P_RANGE}
        for t in range_t:
            results = AlgorithmRunner.run_algorithm_for_param(rng, matrix, algorithms, n, k, t)
            for algorithm, result in results.items():
                if algorithm == algorithm_names.PSI:
                    zipped = list(zip(*result))
                    averages_psi = numpy.average(zipped[0])
                    for p in params.P_RANGE:
                        p_averages[p] = numpy.average(zipped[p])
                    averages[t][algorithm] = [averages_psi, p_averages]
                elif algorithm == algorithm_names.PSI_EUCL_ORD_2 or algorithm == algorithm_names.PSI_EUCL_ORD_5:
                    averages[t][algorithm] = numpy.average(result)
        return averages, k
    except Exception:
        traceback.print_exc()



