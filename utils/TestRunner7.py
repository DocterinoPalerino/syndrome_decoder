import multiprocessing
import time
import traceback
from functools import partial
import utils.params as PARAMS
import numpy as np

from algorithm.SyndromeDecoder import SyndromeDecoder
from algorithm.SyndromeDecoder_v5 import SyndromeDecoder_v5
from database import Database_sd
from utils import SyndromeDecoderSample, MatrixUtilities, algorithm_names
from utils.AlgorithmRunner import run_algorithm_for_param


def prepare_update_psi_phi_alg(rng, matrix, params, database):
    range_k = params[2]
    range_t = params[3]
    n = params[0]

    p = multiprocessing.Pool(4)
    new_func = partial(run_alg_psi_phi, rng, matrix, n, range_t)
    results = p.map(new_func, range_k)
    print(results)
    for results in results:
        k = results[1]
        dict_results = results[0]
        for t, value in dict_results.items():
            # average_psi = value[0]
            # average_phi = value[0]
            average_psi_eucl = value[0]
            average_phi_eucl = value[1]
            average_psi_eucl_ord_5 = value[2]
            average_phi_eucl_ord_5 = value[3]
            # average_subtract_score = value[1]

            column = Database_sd.PSI_EUCL_COLUMN
            database.update_psi_phi_table(column, n, k, t, average_psi_eucl)
            column = Database_sd.PHI_EUCL_COLUMN
            database.update_psi_phi_table(column, n, k, t, average_phi_eucl)
            column = Database_sd.PSI_EUCL_COLUMN_ORD_5
            database.update_psi_phi_table(column, n, k, t, average_psi_eucl_ord_5)
            column = Database_sd.PHI_EUCL_COLUMN_ORD_5
            database.update_psi_phi_table(column, n, k, t, average_phi_eucl_ord_5)


def run_alg(rng, matrix, n, range_t, k):
    try:
        # format -> alg[t][p] = result, time_result
        dict_results = {Database_sd.T_ALG: {t: None for t in range_t},
                        Database_sd.N_K_ALG: {t: None for t in range_t},
                        Database_sd.EXH_SEARCH_ALG: {t: {p: None for p in PARAMS.P_RANGE} for t in range_t}}

        for t in range_t:
            t_results, n_k_results, exh_results, time_results = [], [], [], []
            print(n, k, t)
            error_vectors = SyndromeDecoderSample.get_error_vectors(rng, n, t, PARAMS.TEST_VECTORS)
            sub_matrix = matrix[:k]
            sub_matrix_transposed = np.transpose(sub_matrix)
            sub_matrix_complementary = MatrixUtilities.get_H_complementary(sub_matrix_transposed)

            dict_results_p = {each: [] for each in PARAMS.P_RANGE}
            for e in error_vectors:
                start = time.process_time()
                alg = SyndromeDecoder(sub_matrix, e, t, sub_matrix_transposed, sub_matrix_complementary)
                end = time.process_time() - start
                e_prime = alg.get_permuted_e()

                # it follows that if t_success is 1, then the others also must succeed
                t_success = 1 if np.count_nonzero(e_prime[:t]) == t else 0
                n_k_success = 1 if t_success == 1 else 1 if np.count_nonzero(e_prime[:k]) == t else 0
                for p in PARAMS.P_RANGE:
                    exh_success = 1 if n_k_success == 1 else 1 if np.count_nonzero(e_prime[:k - p]) == t - p else 0
                    dict_results_p[p].append(exh_success)

                t_results.append(t_success)
                n_k_results.append(n_k_success)

                time_results.append(end)

            dict_results[Database_sd.T_ALG][t] = [np.average(t_results), np.average(time_results)]
            dict_results[Database_sd.N_K_ALG][t] = [np.average(n_k_results), np.average(time_results)]
            for p in PARAMS.P_RANGE:
                dict_results[Database_sd.EXH_SEARCH_ALG][t][p] = [np.average(dict_results_p[p]),
                                                                  np.average(time_results)]

        return k, dict_results
    except Exception:
        traceback.print_exc()


def run_alg_psi_phi(rng, matrix, algorithms, n, range_t, k):
    try:
        averages = {t: run_algorithm_for_param(rng, matrix, algorithms, n, k, t) for t in range_t}
        return averages, k
    except Exception:
        traceback.print_exc()


def get_rank_success_result(e, rank, t):
    return 1 if np.count_nonzero(e[:rank]) == t else 0


def get_success_result_exhaustive(e, rank, t):
    results = []
    psi_success = get_rank_success_result(e, rank, t)
    results.append(psi_success)
    for p in PARAMS.P_RANGE:
        # it follows that if rank_success is 1, then the others also must succeed
        exh_success = 1 if psi_success == 1 else 1 if get_rank_success_result(e, rank, t - p) else 0
        results.append(exh_success)
    return results

