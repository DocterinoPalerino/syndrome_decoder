import traceback

import numpy as np

from algorithm import SyndromeDecoder
from utils import algorithm_names, SyndromeDecoderSample, MatrixUtilities, params


def get_eucl_success_chance(e, rank, t):
    restriction = len(e) - rank
    return 1 if np.count_nonzero(e[restriction:]) == t else 0


def get_success_chance(algorithm, e, rank, t):
    if algorithm == algorithm_names.PSI:
        return get_success_result_exhaustive(e, rank, t)
    elif algorithm == algorithm_names.PSI_EUCL_ORD_2 or algorithm == algorithm_names.PSI_EUCL_ORD_5:
        return get_eucl_success_chance(e, rank, t)
    raise ValueError('no algorithm named ', algorithm)


def get_average_results(dict_algorithms):
    try:
        results = {}
        for alg_name, test_results in dict_algorithms.items():
            if alg_name == algorithm_names.PSI:
                p_dict = {p: np.average(test_results[p]) for p in params.P_RANGE}
                results[alg_name] = p_dict
            else:
                raise ValueError('no algorithm named ', alg_name)
            return results
    except Exception:
        traceback.print_exc()


# {psi = {0 : [], 1: []}, other_alg = []}
def init_dict_algorithms(algorithms):
    dict_algorithms = {}
    for alg in algorithms:
        if alg == algorithm_names.PSI:
            psi_dict = {p: [] for p in params.P_RANGE}
            dict_algorithms[alg] = psi_dict
        else:
            dict_algorithms[alg] = []
    return dict_algorithms


def run_algorithm_for_param(rng, matrix, algorithms, nr_tests, n, k, t):
    dict_algorithms = init_dict_algorithms(algorithms)

    print(n, k, t)
    error_vectors = SyndromeDecoderSample.get_error_vectors(rng, n, t, nr_tests)
    sub_matrix = matrix[:k]
    sub_matrix_transposed = np.transpose(sub_matrix)
    sub_matrix_complementary = MatrixUtilities.get_H_complementary(sub_matrix_transposed)

    for i in range(len(error_vectors)):
        for algorithm in algorithms:
            e_permuted, perm = SyndromeDecoder.get_results(algorithm, sub_matrix, error_vectors[i], t,
                                                           sub_matrix_transposed, sub_matrix_complementary)
            permuted_matrix = np.take(sub_matrix, perm, axis=1)
            rank = np.linalg.matrix_rank(permuted_matrix)
            success = get_success_chance(algorithm, e_permuted, rank, t)
            if algorithm == algorithm_names.PSI:
                for p_key, result_value in success.items():
                    dict_algorithms[algorithm][p_key].append(result_value)
            else:
                dict_algorithms[algorithm].append(success)

        # DELETE this if doing something else besides psi
        if i == params.TESTS_BREAKPOINT_THREE:
            partial_psi_results = dict_algorithms[algorithm_names.PSI][0]
            if np.average(partial_psi_results) == 1:
                print("broke off at ", n, k, t)
                break

    return get_average_results(dict_algorithms)


# applies the psi + psi_exh algorithm
# return type {0 = psi, 1 = psi_exh_1_result, 2 = psi_exh_2_result, 3 = psi_exh_3_result}]
def get_success_result_exhaustive(e, rank, t):
    p_results = {}
    # it follows that if rank_success is 1, then the others also must succeed
    p_results[0] = 1 if np.count_nonzero(e[:rank]) == t else 0
    p_results[1] = 1 if p_results[0] == 1 else 1 if np.count_nonzero(e[:rank - 1]) == t - 1 else 0
    p_results[2] = 1 if p_results[1] == 1 else 1 if np.count_nonzero(e[:rank - 2]) == t - 2 else 0
    p_results[3] = 1 if p_results[2] == 1 else 1 if np.count_nonzero(e[:rank - 3]) == t - 3 else 0
    return p_results
