import time

import numpy as np

from utils import SyndromeDecoderSample
from algorithm.SyndromeDecoder_v3 import SyndromeDecoderV3

TESTS = 100

# generates a static matrix and TESTS amount of error vectors, tests these vectors against the matrix
class TestRunner2:

    def __init__(self, n, k, t):
        self.rng = np.random.default_rng()
        self.n = n
        self.k = k
        self.t = t
        self.run_tests()

    def run_tests(self):
        matrix = SyndromeDecoderSample.get_random_matrix(self.n, self.n - self.k)
        error_vectors = self.get_error_vectors()
        results = []
        time_results = []
        for e in error_vectors:
            start = time.process_time()
            decoder_v3 = SyndromeDecoderV3(matrix, e, self.t)
            end = time.process_time() - start
            time_results.append(end)
            result = decoder_v3.get_error_indices()
            are_results_binary = decoder_v3.are_results_binary_t_equal()
            results.append(result)
        success_chance_results = self.get_chance_results(results)
        final_results = np.average(success_chance_results)
        final_time_results = np.average(time_results)

        print("success chance avg ", final_results)
        print("time avg ", final_time_results)

    def get_error_vectors(self):
        base_error_vector = np.zeros(self.n).astype(int)
        base_error_vector[:self.t] = 1
        error_vectors = [
            SyndromeDecoderSample.get_error_vector_v2(base_error_vector, self.rng) for i in range(TESTS)]
        return error_vectors

    def get_chance_results(self, results):
        results = np.array(results)
        chance_results = np.where(results == 0, 1, 0)
        return chance_results
