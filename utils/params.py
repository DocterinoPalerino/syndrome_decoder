import math

N_ONE = 2000
N_TWO = 10000
RANGE_T_ONE = range(round(math.sqrt(N_ONE * math.log(N_ONE, 10))), round(math.sqrt(N_ONE)), -1)
RANGE_T_TWO = range(round(math.sqrt(N_TWO * math.log(N_TWO, 10))), round(math.sqrt(N_TWO)), -1)
K_RANGE_ONE = range(450, 750, 50)

P_RANGE = range(3, -1, -1)

MATRIX_PARAM_ONE = (N_ONE, 700, K_RANGE_ONE, RANGE_T_ONE)
MATRIX_PARAM_TWO = (N_TWO, 3000, range(100, 3100, 100), RANGE_T_TWO)
TEST_VECTORS = 10
PARAMS = (MATRIX_PARAM_ONE, MATRIX_PARAM_TWO)

N_THREE = 1000
RANGE_T_THREE = range(round(math.sqrt(N_THREE * math.log(N_ONE, 10))), round(math.sqrt(N_THREE)), -1)
K_RANGE_THREE = range(100, 200, 50)
TEST_VECTORS_THREE = 1000
TESTS_BREAKPOINT_THREE = TEST_VECTORS_THREE / 2
MATRIX_PARAM_THREE = (N_THREE, 300, K_RANGE_THREE, range(10,20))

