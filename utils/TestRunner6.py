import itertools
import math
import multiprocessing
import time
from functools import partial

import numpy as np

from algorithm.SyndromeDecoder_n_k import SyndromeDecoder_N_K
from algorithm.SyndromeDecoder_n_k_exhaustive import SyndromeDecoder_N_K_Exhaustive
from algorithm.SyndromeDecoder_t import SyndromeDecoder_T
from database.Database_sd import T_ALG, N_K_ALG, EXH_SEARCH_ALG
from utils import SyndromeDecoderSample

N_ONE = 2000
N_TWO = 10000
RANGE_T_ONE = range(round(math.sqrt(N_ONE * math.log(N_ONE, 10))), round(math.sqrt(N_ONE)), -1)
RANGE_T_TWO = range(round(math.sqrt(N_TWO * math.log(N_TWO, 10))), round(math.sqrt(N_TWO)), -1)

P_RANGE = range(3, 0, -1)

MATRIX_PARAM_ONE = (N_ONE, 700, range(50, 750, 50), RANGE_T_ONE)
MATRIX_PARAM_ONE_EXH = (N_ONE, 700, range(250, 750, 50), RANGE_T_ONE)

MATRIX_PARAM_TWO = (N_TWO, 3000, range(100, 3100, 100), RANGE_T_TWO)
TEST_VECTORS = 1000
PARAMS = (MATRIX_PARAM_ONE, MATRIX_PARAM_TWO)


def run_t_alg(rng, matrix, params, database):
    range_k = params[2]
    range_t = params[3]
    n = params[0]

    p = multiprocessing.Pool()
    for k in range_k:
        new_func = partial(run_t_alg_for_t, rng, matrix, n, k)
        results = p.map(new_func, range_t)
        for each in results:
            database.insert_to_db(T_ALG, n, k, each[0], each[1], each[2])


def run_t_alg_for_t(rng, matrix, n, k, t):
    results, time_results = [], []
    error_vectors = get_error_vectors(rng, n, t)
    sub_matrix = matrix[:k]

    for e in error_vectors:
        start = time.process_time()
        t_alg = SyndromeDecoder_T(sub_matrix, e, t)
        end = time.process_time() - start
        results.append(t_alg.get_result_chances())
        time_results.append(end)
    final_results = np.average(results)
    final_time_results = np.average(time_results)
    return [t, final_results, final_time_results]


def run_n_k_alg(rng, matrix, params, database):
    range_k = params[2]
    range_t = params[3]
    n = params[0]

    p = multiprocessing.Pool()
    for k in range_k:
        new_func = partial(run_n_k_alg, rng, matrix, n, k)
        results = p.map(new_func, range_t)
        for each in results:
            database.insert_to_db(N_K_ALG, n, k, each[0], each[1], each[2])


def run_n_k_alg_for_t(rng, matrix, n, k, t):
    results, time_results = [], []
    error_vectors = get_error_vectors(rng, n, t)
    sub_matrix = matrix[:k]
    for e in error_vectors:
        start = time.process_time()
        n_k_alg = SyndromeDecoder_N_K(sub_matrix, e, t)
        end = time.process_time() - start
        results.append(n_k_alg.get_result_chances())
        time_results.append(end)
    final_results = np.average(results)
    final_time_results = np.average(time_results)
    return [t, final_results, final_time_results]


def run_exh_alg(rng, matrix, params, database):
    range_k = params[2]
    range_t = params[3]
    n = params[0]

    p = multiprocessing.Pool(4)
    for k in range_k:
        new_func = partial(run_exh_alg_for_t, rng, matrix, n, k)
        p_dict = p.map(new_func, range_t)
        for dict_value in p_dict:
            for key, value in dict_value.items():
                database.insert_to_db_exh(EXH_SEARCH_ALG, n, k, value[0], key, value[1], value[2])


def run_exh_alg_for_t(rng, matrix, n, k, t):
    p_dict = {}
    for p in P_RANGE:
        combinations = set(itertools.combinations(range(n-k), p))
        print(len(combinations))
        results, time_results = [], []
        error_vectors = get_error_vectors(rng, n, t)
        sub_matrix = matrix[:k]
        for e in error_vectors:
            start = time.process_time()
            exh_alg = SyndromeDecoder_N_K_Exhaustive(sub_matrix, e, t, p, combinations)
            end = time.process_time() - start
            results.append(exh_alg.get_result_chances())
            time_results.append(end)
        final_results = np.average(results)
        final_time_results = np.average(time_results)
        print(n, k, t, p, final_results, final_time_results)
        p_dict[p] = [t, final_results, final_time_results]
    return p_dict


def prepare_tests_one(database):
    n = MATRIX_PARAM_ONE[0]
    k = MATRIX_PARAM_ONE[1]

    rng = np.random.default_rng()
    matrix = SyndromeDecoderSample.get_random_matrix(n, k)

    # run_t_alg(rng, matrix, MATRIX_PARAM_TEST, database)
    # run_n_k_alg(rng, matrix, MATRIX_PARAM_TEST, database)
    run_exh_alg(rng, matrix, MATRIX_PARAM_ONE, database)


def prepare_tests_two(database):
    n = MATRIX_PARAM_TWO[0]
    k = MATRIX_PARAM_TWO[1]

    rng = np.random.default_rng()
    matrix = SyndromeDecoderSample.get_random_matrix(n, k)

    run_t_alg(rng, matrix, MATRIX_PARAM_TWO, database)
    run_n_k_alg(rng, matrix, MATRIX_PARAM_TWO, database)
    run_exh_alg(rng, matrix, MATRIX_PARAM_TWO, database)


def prepare_tests(database):
    prepare_tests_one(database)
    prepare_tests_two(database)


def get_error_vectors(rng, n, t):
    base_error_vector = np.zeros(n).astype(int)
    base_error_vector[:t] = 1
    error_vectors = [
        SyndromeDecoderSample.get_error_vector_v2(base_error_vector, rng) for i in range(TEST_VECTORS)]
    return error_vectors
