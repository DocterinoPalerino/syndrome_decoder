import sqlite3

T_ALG = 't_alg'
N_K_ALG = 'n_k_alg'
EXH_SEARCH_ALG = 'exh_alg'
NIST_TABLE = 'nist_table'
N_K_ALG_PHI_PSI_TESTS = 'n_k_alg_PHI_PSI_tests'
PSI_AND_PSI_EUCL_TABLE = 'psi_and_psi_eucl'
PSI_EXH_TABLE = 'psi_exh'

N_COLUMN = 'n'
K_COLUMN = 'k'
T_COLUMN = 't'
SUCCESS_AVG_RESULTS_COLUMN = 'results_success_avg'
TIME_AVG_RESULTS_COLUMN = 'results_time_avg'
P_COLUMN = 'p'
ALG_NAME_COLUMN = 'alg_name'

PSI_COLUMN = 'psi'
PHI_COLUMN = 'phi'
PSI_EUCL_COLUMN = 'psi_eucl'
PHI_EUCL_COLUMN = 'phi_eucl'
SUBTRACT_COLUMN = 'subtract_score'
PSI_EUCL_COLUMN_ORD_5 = 'psi_eucl_ord_5'
PHI_EUCL_COLUMN_ORD_5 = 'phi_eucl_ord_5'


class Database:

    def __init__(self):
        self.conn = self.create_connection("database/scores.db")
        self.create_tables()

        # self.conn.commit()
        # self.conn.close()

    def insert_to_db(self, table, n, k, t, chance_result, time_result):
        self.run_statement('INSERT INTO %s VALUES(?, ?, ?, ?, ?)' % table, (n, k, t, chance_result, time_result))

    def insert_to_db_exh(self, table, n, k, t, p, chance_result, time_result):
        self.run_statement('INSERT INTO %s VALUES(?, ?, ?, ?, ?, ?)' % table, (n, k, t, p, chance_result, time_result))

    def insert_to_nist_table(self, alg_name, n, k, t, chance_result, time_result):
        self.run_statement('INSERT INTO %s(%s, %s, %s, %s, %s, %s) VALUES(?, ?, ?, ?, ?, ?)'
                           % (NIST_TABLE, ALG_NAME_COLUMN, N_COLUMN, K_COLUMN, T_COLUMN,
                              SUCCESS_AVG_RESULTS_COLUMN, TIME_AVG_RESULTS_COLUMN),
                           (alg_name, n, k, t, chance_result, time_result))

    def insert_to_nist_p_table(self, alg_name, n, k, t, p, chance_result):
        self.run_statement('INSERT INTO %s VALUES(?, ?, ?, ?, ?, ?)' % NIST_TABLE,
                           (alg_name, n, k, t, p, chance_result))

    def insert_to_phi_psi_table(self, n, k, t, psi_chance_result, phi_chance_result, psi_eucl_chance_result,
                                phi_eucl_chance_result, subtract_chance_result):
        self.run_statement('INSERT INTO %s(%s,%s,%s,%s,%s,%s,%s,%s) VALUES(?, ?, ?, ?, ?, ?, ?, ?)' %
                           (N_K_ALG_PHI_PSI_TESTS, N_COLUMN, K_COLUMN, T_COLUMN,
                            PSI_COLUMN, PHI_COLUMN, PSI_EUCL_COLUMN, PHI_EUCL_COLUMN, SUBTRACT_COLUMN),
                           (n, k, t, psi_chance_result, phi_chance_result, psi_eucl_chance_result,
                            phi_eucl_chance_result, subtract_chance_result))

    def run_statement(self, statement, tuple_values):
        c = self.conn.cursor()
        try:
            c.execute(statement, tuple_values)
            self.conn.commit()
        except sqlite3.Error as e:
            print('sqlite error', e)

    def create_connection(self, db_file):
        """ create a database connection to a SQLite database """
        try:
            conn = sqlite3.connect(db_file)
            return conn
        except sqlite3.Error as e:
            print(e)
        return None

    def create_tables(self):
        create_table_t_alg = """CREATE TABLE IF NOT EXISTS %s (
                                        %s integer,
                                        %s integer,
                                        %s integer, 
                                        %s real,
                                        %s real
                                    );""" % (T_ALG, N_COLUMN, K_COLUMN, T_COLUMN,
                                             SUCCESS_AVG_RESULTS_COLUMN, TIME_AVG_RESULTS_COLUMN)

        create_table_n_k_alg = """CREATE TABLE IF NOT EXISTS %s (
                                                %s integer,
                                                %s integer,
                                                %s integer, 
                                                %s real,
                                                %s real
                                            );""" % (N_K_ALG, N_COLUMN, K_COLUMN, T_COLUMN,
                                                     SUCCESS_AVG_RESULTS_COLUMN, TIME_AVG_RESULTS_COLUMN)

        create_table_exh_alg = """CREATE TABLE IF NOT EXISTS %s (
                                                %s integer,
                                                %s integer,
                                                %s integer, 
                                                %s integer,
                                                %s real,
                                                %s real
                                            );""" % (EXH_SEARCH_ALG, N_COLUMN, K_COLUMN, T_COLUMN,
                                                     P_COLUMN, SUCCESS_AVG_RESULTS_COLUMN, TIME_AVG_RESULTS_COLUMN)

        create_table_nist = """CREATE TABLE IF NOT EXISTS %s (
                                                        %s text,
                                                        %s integer,
                                                        %s integer, 
                                                        %s integer,
                                                        %s integer,
                                                        %s real,
                                                        %s real
                                                    );""" % (NIST_TABLE, ALG_NAME_COLUMN, N_COLUMN, K_COLUMN, T_COLUMN,
                                                             P_COLUMN, SUCCESS_AVG_RESULTS_COLUMN,
                                                             TIME_AVG_RESULTS_COLUMN)

        create_table_n_k_phi_psi = """CREATE TABLE IF NOT EXISTS %s (
                                                                %s integer,
                                                                %s integer,
                                                                %s integer, 
                                                                %s real,
                                                                %s real,
                                                                %s real,
                                                                %s real,
                                                                %s real
                                                            );""" % (
            N_K_ALG_PHI_PSI_TESTS, N_COLUMN, K_COLUMN, T_COLUMN,
            PHI_COLUMN, PSI_COLUMN, PHI_EUCL_COLUMN, PSI_EUCL_COLUMN, SUBTRACT_COLUMN)

        create_table_psi_and_psi_eucl = """CREATE TABLE IF NOT EXISTS %s (
                                                                        %s integer,
                                                                        %s integer,
                                                                        %s integer, 
                                                                        %s real,
                                                                        %s real,
                                                                        %s real
                                                                    );""" % (
            PSI_AND_PSI_EUCL_TABLE, N_COLUMN, K_COLUMN, T_COLUMN,
            PSI_COLUMN, PSI_EUCL_COLUMN, PSI_EUCL_COLUMN_ORD_5)

        create_table_psi_exh = """CREATE TABLE IF NOT EXISTS %s (  
                                                                   %s integer,
                                                                   %s integer,
                                                                   %s integer, 
                                                                   %s integer, 
                                                                   %s real
                                                                   );""" % (
            PSI_EXH_TABLE, N_COLUMN, K_COLUMN, T_COLUMN, P_COLUMN, PSI_COLUMN)

        c = self.conn.cursor()

        c.execute(create_table_t_alg)
        c.execute(create_table_n_k_alg)
        c.execute(create_table_exh_alg)
        c.execute(create_table_nist)
        c.execute(create_table_n_k_phi_psi)
        c.execute(create_table_psi_and_psi_eucl)
        c.execute(create_table_psi_exh)

    def insert_test_values(self):
        c = self.conn.cursor()
        c.execute('INSERT INTO %s VALUES(?, ?, ?, ?, ?)' % T_ALG, (2000, 450, 100, 0.53256, 55.5))

    def read_results_for(self, alg_name, n):
        c = self.conn.cursor()
        statement = 'SELECT * FROM %s where %s = ?' % (alg_name, N_COLUMN)
        c.execute(statement, (n,))

        rows = c.fetchall()
        return rows

    def read_results_for_p_range(self, alg_name, n, p, k_range, t_range):
        c = self.conn.cursor()
        statement = 'SELECT * FROM %s where %s = ? and %s = ? AND %s BETWEEN ? AND ? AND %s BETWEEN ? AND ?' % (
            alg_name, N_COLUMN, P_COLUMN, K_COLUMN, T_COLUMN)
        c.execute(statement, (n, p, k_range[0], k_range[1], t_range[0], t_range[1]))

        rows = c.fetchall()
        return rows

    def read_results_for_k_t_range(self, alg_name, n, k_range, t_range):
        c = self.conn.cursor()
        statement = 'SELECT * FROM %s where %s = ? AND %s BETWEEN ? AND ? AND %s BETWEEN ? AND ?' % (
            alg_name, N_COLUMN, K_COLUMN, T_COLUMN)
        c.execute(statement, (n, k_range[0], k_range[1], t_range[0], t_range[1]))

        rows = c.fetchall()
        return rows

    def read_results_for_k_range(self, alg_name, n, k_range):
        c = self.conn.cursor()
        statement = 'SELECT * FROM %s where %s = ? AND %s BETWEEN ? AND ?' % (alg_name, N_COLUMN, K_COLUMN)
        c.execute(statement, (n, k_range[0], k_range[1]))

        rows = c.fetchall()
        return rows

    def update_psi_phi_table(self, column, n, k, t, average):
        c = self.conn.cursor()
        statement = 'UPDATE %s set %s = ? WHERE %s = ? AND %s = ? AND %s = ?' \
                    % (N_K_ALG_PHI_PSI_TESTS, column, N_COLUMN, K_COLUMN, T_COLUMN)
        c.execute(statement, (average, n, k, t))
        self.conn.commit()

    def add_table_column(self, table, column, column_def):
        statement = 'ALTER TABLE %s ADD %s %s' % (table, column, column_def)
        c = self.conn.cursor()
        c.execute(statement)
        self.conn.commit()

    def read_psi_phi_euclidian_results(self):
        c = self.conn.cursor()
        statement = 'SELECT %s,%s,%s,%s,%s,%s FROM %s' % \
                    (K_COLUMN, T_COLUMN,
                     PHI_EUCL_COLUMN, PSI_EUCL_COLUMN,
                     PHI_EUCL_COLUMN_ORD_5, PSI_EUCL_COLUMN_ORD_5, N_K_ALG_PHI_PSI_TESTS)
        c.execute(statement)

        rows = c.fetchall()
        return rows

    def read_results_for_column(self, table, column):
        c = self.conn.cursor()
        statement = 'SELECT %s,%s,%s,%s FROM %s' % (N_COLUMN, K_COLUMN, T_COLUMN, column, table)
        c.execute(statement)

        return c.fetchall()

    def insert_to_psi_and_psi_eucl(self, n, k, t, psi_success_chance, psi_eucl_success_chance,
                                   psi_eucl_5_success_chance):
        self.run_statement('INSERT INTO %s VALUES(?, ?, ?, ?, ?, ?)' % PSI_AND_PSI_EUCL_TABLE,
                           (n, k, t, psi_success_chance, psi_eucl_success_chance, psi_eucl_5_success_chance))

    def insert_to_psi_and_psi_eucl_p(self, n, k, t, p, psi_success_chance):
        self.run_statement('INSERT INTO %s VALUES(?, ?, ?, ?, ?)' % PSI_EXH_TABLE,
                           (n, k, t, p, psi_success_chance))

    def read_results_for_p(self, alg_name, p):
        c = self.conn.cursor()
        statement = 'SELECT * FROM %s where %s = ?' % (alg_name, P_COLUMN)
        c.execute(statement, (str(p),))

        rows = c.fetchall()
        return rows

    def insert_to_table_p(self, table, alg_name, n, k, t, p, success):
        self.run_statement('INSERT INTO %s VALUES(?, ?, ?, ?, ?,?)' % table,
                           (alg_name, n, k, t, p, success))
