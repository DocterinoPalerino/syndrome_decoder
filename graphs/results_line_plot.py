import matplotlib.pyplot as plt


def build_line_plot(title, x_values, y_values, x_label, y_label):
    plt.plot(x_values, y_values)
    plt.xlabel(x_label)
    plt.ylabel(y_label)

    plt.title(title)
    plt.show()


def build_multiple_lines(matrix_dict):
    for matrix_key, matrix_value in matrix_dict.items():
        plt.plot(matrix_value.keys(), matrix_value.values(), label='n = ' + str(matrix_key))
    plt.xlabel('t')
    plt.ylabel('%')
    plt.legend()
    plt.show()


def build_k_time_range(keys, results_percentage, results_time):
    plt.plot(keys, results_percentage, label='avg. prob. of success')
    plt.plot(keys, results_time, label='avg. time')
    plt.xlabel('k')
    plt.ylabel('Probability')
    plt.legend()
    plt.show()


def build_k_optimized_vs_unoptimized(results_percentage):
    items = results_percentage.items()
    results_optimized, results_unoptimized = [], []
    for key, value in items:
        results_unoptimized.append(value[0])
        results_optimized.append(value[1])

    plt.plot(results_percentage.keys(), results_optimized, label='Exhaustive Search & (n-k)-threshold')
    plt.plot(results_percentage.keys(), results_unoptimized, label='(n-k)-threshold')
    plt.xlabel('k')
    plt.ylabel('avg. prob. of success')
    plt.legend()
    plt.show()


def build_time(results_time):
    items = results_time.items()
    results_optimized, results_unoptimized = [], []
    for key, value in items:
        results_unoptimized.append(value[0])
        results_optimized.append(value[1])

    plt.plot(results_time.keys(), results_optimized, label='Exhaustive Search & (n-k)-threshold')
    plt.plot(results_time.keys(), results_unoptimized, label='(n-k)-threshold')
    plt.xlabel('k')
    plt.ylabel('sec.')
    plt.legend()
    plt.show()


def build_time_comparison_3_vs_6(results_v3, results_v6):
    plt.plot(results_v3.keys(), results_v3.values(), label='(n-k)-threshold')
    plt.plot(results_v6.keys(), results_v6.values(), label='(n-k)-threshold optimization')
    plt.xlabel('k')
    plt.ylabel('sec.')
    plt.legend()
    plt.show()