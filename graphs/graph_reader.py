from itertools import islice

import matplotlib.pyplot as plt
import numpy

import graphs.results_line_plot as plot_builder
from database import Database_sd
from database.Database_sd import Database

FILE = 'graphs/results_25_may.txt'


def show_results_from_file_range_k():
    with open(FILE) as f:
        lines = f.readlines()
    L = {}
    for line in lines:
        line_values = line.removeprefix('[').removesuffix(']')
        split_values = line_values.split(', ')
        for split in split_values:
            split_sub_value = split.removeprefix('{').removesuffix('}')
            values = split_sub_value.split(':')
            L[int(values[0])] = float(values[1])
    plot_builder.build_line_plot('Score Algorithm Success Chance for increasing k', L.keys(), L.values(), 'k', '%')


def show_results_from_file_range_t():
    with open(FILE) as f:
        lines = f.readlines()

    matrix_dict = {}
    for line in lines:
        matrix_values = line.split(':', 1)
        matrix_sub_values = matrix_values[1].removeprefix(' {').removesuffix('\n').removesuffix(',').removesuffix(
            '}')
        t_values = matrix_sub_values.split(',')
        t_subdict = {}
        for t_value in t_values:
            test_value = t_value.split(':')
            t_subdict[int(test_value[0])] = float(test_value[1])
        matrix_dict[int(matrix_values[0])] = t_subdict

    plot_builder.build_multiple_lines(matrix_dict)


def show_results_from_file_range_k_slice():
    with open(FILE) as f:
        lines = f.readlines()

    results_percentage = {}
    results_time = {}
    for each in lines:
        values_str = each.removeprefix('{').removesuffix('}\n')
        values = values_str.split('],')
        for each_word in values:
            key_value = each_word.split(': ')

            if int(key_value[0]) not in results_percentage:
                results_percentage[int(key_value[0])] = []
                results_time[int(key_value[0])] = []

            results = key_value[1].removeprefix('[').removesuffix(']').split(',')
            results_percentage[int(key_value[0])].append(float(results[0]))
            results_time[int(key_value[0])].append(float(results[1]))

    plot_builder.build_k_optimized_vs_unoptimized(results_percentage)
    plot_builder.build_time(results_time)


def show_results_time_comparison_3_vs_6():
    with open(FILE) as f:
        line = f.readlines()

    results_v3 = {}
    results_v6 = {}

    values_str = line[0].removeprefix('{').removesuffix('}\n')
    values = values_str.split('],')
    for each_word in values:
        key_value = each_word.split(': ')

        results = key_value[1].removeprefix('[').removesuffix(']').split(',')
        results_v3[int(key_value[0])] = float(results[0])
        results_v6[int(key_value[0])] = float(results[1])

    plot_builder.build_time_comparison_3_vs_6(results_v3, results_v6)


def show_results_form_3d_plot():
    database = Database()
    n = 10000
    alg_name = Database_sd.N_K_ALG
    k_range = (1000, 3100)
    t_range = (100, 200)
    results = database.read_results_for_k_t_range(alg_name, n, k_range, t_range)
    results_exh_1 = database.read_results_for_p(Database_sd.EXH_SEARCH_ALG, n, 1, k_range, t_range)
    results_exh_2 = database.read_results_for_p(Database_sd.EXH_SEARCH_ALG, n, 2, k_range, t_range)
    results_exh_3 = database.read_results_for_p(Database_sd.EXH_SEARCH_ALG, n, 3, k_range, t_range)

    zipped = list(zip(*results))
    zipped2 = list(zip(*results_exh_1))
    zipped3 = list(zip(*results_exh_2))
    zipped4 = list(zip(*results_exh_3))

    k_values = zipped[1]
    t_values = zipped[2]
    chance_results = numpy.array(zipped[3])
    chance_results_exh_1 = numpy.array(zipped2[4])
    chance_results_exh_2 = numpy.array(zipped3[4])
    chance_results_exh_3 = numpy.array(zipped4[4])

    k_values_set = list(set(k_values))
    t_values_set = list(set(t_values))
    k_values_set = sorted(k_values_set)
    t_values_set = sorted(t_values_set, reverse=True)

    x, y = numpy.meshgrid(t_values_set, k_values_set)

    fig = plt.figure()
    ax = fig.add_subplot(projection='3d')
    # ax = fig.gca(projection='3d')

    z = numpy.array(chance_results)
    z = z.reshape(len(k_values_set), len(t_values_set))
    ax.scatter(x.ravel(), y.ravel(), z.ravel(), marker='o', color='black')
    ax.plot_surface(x, y, z, color='black', alpha=1)

    # x, y, z = numpy.broadcast_arrays(k_values, t_values, chance_results_exh_1)

    z = numpy.array(chance_results_exh_1)
    z = z.reshape(len(k_values_set), len(t_values_set))
    ax.scatter(x.ravel(), y.ravel(), z.ravel(), marker='x', color='green')
    ax.plot_surface(x, y, z, color='green', alpha=0.1)

    z = numpy.array(chance_results_exh_2)
    z = z.reshape(len(k_values_set), len(t_values_set))
    ax.scatter(x.ravel(), y.ravel(), z.ravel(), marker='v', color='yellow')
    ax.plot_surface(x, y, z, color='yellow', alpha=0.5)

    # x, y, z = numpy.broadcast_arrays(k_values, t_values, chance_results_exh_2)
    #
    # ax.scatter(x.ravel(),
    #            y.ravel(),
    #            z.ravel(), marker='s')
    #
    # x, y, z = numpy.broadcast_arrays(k_values, t_values, chance_results_exh_3)
    #
    # ax.scatter(x.ravel(),
    #            y.ravel(),
    #            z.ravel(), marker='^')
    # ax2_EXH_1 = fig.add_subplot(projection='3d')
    # ax2_EXH_1.scatter(x.ravel(),
    #                y.ravel(),
    #                z.ravel())

    plt.show()
    fig.show()


def get_data_3d_repr():
    database = Database()
    n = 2000
    k_range = (450, 750)
    alg_name = Database_sd.N_K_ALG
    results = database.read_results_for_k_range(alg_name, n, k_range)

    alg_name = Database_sd.N_K_ALG_PHI_PSI_TESTS
    results_second = database.read_results_for_k_range(alg_name, n, k_range)

    zipped = list(zip(*results))
    zipped2 = list(zip(*results_second))

    k_values = zipped[1]
    t_values = zipped[2]
    chance_results = numpy.array(zipped[3])
    chance_results_psi_2 = numpy.array(zipped2[6])
    chance_results_psi_5 = numpy.array(zipped2[9])

    k_values_set = list(set(k_values))
    t_values_set = list(set(t_values))
    k_values_set = sorted(k_values_set)
    t_values_set = sorted(t_values_set)

    return chance_results, chance_results_psi_2, chance_results_psi_5, k_values_set, t_values_set


def show_results_form_3d_plot_contour():
    chance_results, chance_results_psi_2, chance_results_psi_5, k_values_set, t_values_set = get_data_3d_repr()

    x, y = numpy.meshgrid(t_values_set, k_values_set)

    z1 = numpy.array(chance_results)
    z1 = z1.reshape(len(k_values_set), len(t_values_set))

    z2 = numpy.array(chance_results_psi_2)
    z2 = z2.reshape(len(k_values_set), len(t_values_set))

    z3 = numpy.array(chance_results_psi_5)
    z3 = z3.reshape(len(k_values_set), len(t_values_set))

    fig = plt.figure()
    ax = fig.add_subplot(projection='3d')
    # ax.plot_surface(x, y, z1, rstride=1, cstride=1,
    #                 cmap='viridis', edgecolor='none')

    # ax.plot_wireframe(x, y, z2, color='red')
    # ax.plot_wireframe(x, y, z1, color='black')

    ax.plot3D(x, y, z1, 'gray')

    ax.scatter(x.ravel(), y.ravel(), z1.ravel(), marker='o', label='psi', color='black')
    ax.scatter(x.ravel(), y.ravel(), z2.ravel(), marker='v', label='eucl_psi_ord_2', color='blue')
    ax.scatter(x.ravel(), y.ravel(), z3.ravel(), marker='x', label='eucl_psi_ord_5', color='red')

    # plt.legend(loc='upper center', bbox_to_anchor=(0.5, -0.05),
    #            fancybox=True, shadow=True, ncol=4)

    fig.show()
    plt.show()


def show_results_for_euclidian_tests():
    database = Database()
    results = database.read_psi_phi_euclidian_results()

    zipped = list(zip(*results))

    k_values = zipped[0]
    t_values = zipped[1]
    phi_eucl_results = numpy.array(zipped[2])
    psi_eucl_results = numpy.array(zipped[3])
    phi_eucl_ord5_results = numpy.array(zipped[4])
    psi_eucl_ord5_results = numpy.array(zipped[5])

    k_values_set = list(set(k_values))
    t_values_set = list(set(t_values))
    k_values_set = sorted(k_values_set)
    t_values_set = sorted(t_values_set, reverse=True)

    x, y = numpy.meshgrid(t_values_set, k_values_set)

    z1 = numpy.array(phi_eucl_results)
    z1 = z1.reshape(len(k_values_set), len(t_values_set))

    z2 = numpy.array(psi_eucl_results)
    z2 = z2.reshape(len(k_values_set), len(t_values_set))

    z3 = numpy.array(phi_eucl_ord5_results)
    z3 = z3.reshape(len(k_values_set), len(t_values_set))

    z4 = numpy.array(psi_eucl_ord5_results)
    z4 = z4.reshape(len(k_values_set), len(t_values_set))

    fig = plt.figure()
    ax = fig.add_subplot(projection='3d')

    # ax.scatter(x.ravel(), y.ravel(), z1.ravel(), marker='o', label='eucl_phi', color='green')
    ax.scatter(x.ravel(), y.ravel(), z2.ravel(), marker='x', color='blue', label='eucl_psi', s=50)
    # ax.scatter(x.ravel(), y.ravel(), z3.ravel(), marker='s', label='eucl_phi_ord_5', color='orange')
    ax.scatter(x.ravel(), y.ravel(), z4.ravel(), marker='v', label='eucl_psi_ord_5', color='red', s=50)

    # ax.contour3D(x, y, z1, 50, cmap='binary', color='blue')
    # ax.contour3D(x, y, z2, 50, cmap='binary')
    # ax.contour3D(x, y, z4, 50, cmap='binary')
    # ax.contour3D(x, y, z2, 50, cmap='binary')

    # ax.plot_wireframe(x, y, z4, color='red', alpha = 0.5)
    # ax.plot_wireframe(x, y, z2, color='blue', alpha = 0.5)

    ax.plot_surface(x, y, z2, color='blue', alpha=0.2)
    ax.plot_surface(x, y, z4, color='red', alpha=0.5)

    # ax.plot_trisurf(x,y,z2 )
    # ax.plot_trisurf(x,y,z4 )

    # plt.legend(loc='upper center', bbox_to_anchor=(0.5, -0.05),
    #            fancybox=True, shadow=True, ncol=4)
    fig.show()
    plt.show()


def show_results_form_line_plot():
    database = Database()
    results_PSI = database.read_results_for_column(Database_sd.PSI_AND_PSI_EUCL_TABLE,
                                                   Database_sd.PSI_COLUMN)
    results_PSI_EUCL_2 = database.read_results_for_column(Database_sd.PSI_AND_PSI_EUCL_TABLE,
                                                          Database_sd.PSI_EUCL_COLUMN)

    results_PSI_EUCL_5 = database.read_results_for_column(Database_sd.PSI_AND_PSI_EUCL_TABLE,
                                                          Database_sd.PSI_EUCL_COLUMN_ORD_5)

    results_PSI_EXH_1 = database.read_results_for_p(Database_sd.PSI_EXH_TABLE, 1)
    results_PSI_EXH_2 = database.read_results_for_p(Database_sd.PSI_EXH_TABLE, 2)
    results_PSI_EXH_3 = database.read_results_for_p(Database_sd.PSI_EXH_TABLE, 3)

    psi_zipped = list(zip(*results_PSI))
    psi_eucl_2_zipped = list(zip(*results_PSI_EUCL_2))
    psi_eucl_5_zipped = list(zip(*results_PSI_EUCL_5))
    psi_exh_1_zipped = list(zip(*results_PSI_EXH_1))
    psi_exh_2_zipped = list(zip(*results_PSI_EXH_2))
    psi_exh_3_zipped = list(zip(*results_PSI_EXH_3))

    k_values = psi_zipped[1]
    t_values = psi_zipped[2]

    psi_results = numpy.array(psi_zipped[3])
    psi_eucl_2_results = numpy.array(psi_eucl_2_zipped[3])
    psi_eucl_5_results = numpy.array(psi_eucl_5_zipped[3])
    psi_exh_1_results = numpy.array(psi_exh_1_zipped[4])
    psi_exh_2_results = numpy.array(psi_exh_2_zipped[4])
    psi_exh_3_results = numpy.array(psi_exh_3_zipped[4])

    t_chunks = len(k_values) / len(set(t_values))
    k_values_split = numpy.array_split(numpy.array(k_values), t_chunks)
    t_values_split = numpy.array_split(numpy.array(t_values), t_chunks)
    psi_split = numpy.array_split(numpy.array(psi_results), t_chunks)
    psi_eucl_2_split = numpy.array_split(numpy.array(psi_eucl_2_results), t_chunks)
    psi_eucl_5_split = numpy.array_split(numpy.array(psi_eucl_5_results), t_chunks)
    psi_exh_1_split = numpy.array_split(numpy.array(psi_exh_1_results), t_chunks)
    psi_exh_2_split = numpy.array_split(numpy.array(psi_exh_2_results), t_chunks)
    psi_exh_3_split = numpy.array_split(numpy.array(psi_exh_3_results), t_chunks)

    k_chunks = len(set(k_values))
    k_values_split_k = list(zip(*k_values_split))
    t_values_split_k = list(zip(*t_values_split))
    psi_split_k = list(zip(*psi_split))
    psi_eucl_2_split_k = list(zip(*psi_eucl_2_split))
    psi_eucl_5_split_k = list(zip(*psi_eucl_5_split))

    ax = plt.axes(projection='3d')

    # for i in range(int(t_chunks)):
    #     ax.plot3D(k_values_split[i], t_values_split[i], psi_split[i], 'black')
        # ax.plot3D(k_values_split[i], t_values_split[i], psi_eucl_2_split[i], 'red')
        # ax.plot3D(k_values_split[i], t_values_split[i], psi_eucl_5_split[i], 'green')
        # ax.plot3D(k_values_split[i], t_values_split[i], psi_exh_3_split[i], 'blue')

    for i in range(len(k_values_split_k)):
        ax.plot3D(k_values_split_k[i], t_values_split_k[i], psi_split_k[i], 'black')
        ax.plot3D(k_values_split_k[i], t_values_split_k[i], psi_eucl_2_split_k[i], 'red')
        ax.plot3D(k_values_split_k[i], t_values_split_k[i], psi_eucl_5_split_k[i], 'green')
        # ax.plot3D(k_values_split[i], t_values_split[i], psi_exh_3_split[i], 'blue')

    ax.scatter(k_values, t_values, psi_results, marker='x', color='black', label='eucl_psi', s=50)
    ax.scatter(k_values, t_values, psi_eucl_2_results, marker='v', label='eucl_2', color='red', s=50)
    ax.scatter(k_values, t_values, psi_eucl_5_results, marker='o', label='eucl_5', color='green', s=50)
    # ax.scatter(k_values, t_values, psi_exh_3_results, marker='s', label='eucl_exh_3', color='blue', s=50)

    # plt.legend(loc='upper center', bbox_to_anchor=(0.5, -0.05),
    #            fancybox=True, shadow=True, ncol=4)

    plt.show()
