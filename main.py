from database.Database_sd import Database
from graphs import graph_reader
from utils import TestRunner8, params, NistTestRunner


def run():
    database = Database()

    NistTestRunner.prepare_nist_tests(database)
    # graph_reader.show_results_form_line_plot()
    # graph_reader.show_results_form_3d_plot()


if __name__ == '__main__':
    run()
